<?php

namespace Wai\Adminify\Commands;

class SetupCommand extends BaseCommand
{
    protected $signature = 'adminify:setup';

    protected $description = 'Basic integration of adminify with Laravel application.';

    protected $step = 0;
    protected $totalSteps = 14;

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $this->info('Starting setup');

        $defaultFiles = __DIR__ . '/../../stubs/';

        // 1. Initialise Git and copy over a default .gitignore for Git purposes.
        exec('git init');
        exec('cp ' . $defaultFiles . 'gitignore.stub ' . base_path('.gitignore'));

        $this->updateStep();

        // 2. Update env files
        exec('cp ' . $defaultFiles . 'env.stub ' . base_path('.env'));
        exec('cp ' . $defaultFiles . 'env.stub ' . base_path('.env.example'));

        $this->artisan('key:generate');

        $this->updateStep();

        // 3. Update configuration files

        exec('cp ' . $defaultFiles . 'Config/app.php.stub ' . config_path('app.php'));
        exec('cp ' . $defaultFiles . 'Config/auth.php.stub ' . config_path('auth.php'));

        $this->updateStep();

        // 4. Enable error logging feature

        exec('cp ' . $defaultFiles . 'Exceptions/Handler.php.stub ' . app_path('Exceptions/Handler.php'));

        $this->updateStep();

        // 5. Update migrations

        exec('rm -R ' . database_path('migrations/*'));
        exec('cp ' . $defaultFiles . 'Database/migrations/_000000_create_users_table.php.stub ' . database_path('migrations/' . date('Y_m_d') . '_000000_create_users_table.php'));
        exec('cp ' . $defaultFiles . 'Database/migrations/_100000_create_password_resets_table.php.stub ' . database_path('migrations/' . date('Y_m_d') . '_100000_create_password_resets_table.php'));
        exec('cp ' . $defaultFiles . 'Database/factories/UserFactory.php.stub ' . database_path('factories/' . 'UserFactory.php'));

        $this->artisan('queue:table');
        sleep(1);
        $this->artisan('queue:failed-table');
        sleep(1);
        $this->artisan('session:table');
        sleep(1);

        $this->updateStep();

        // 6. Update model

        exec('cp ' . $defaultFiles . 'Models/User.php.stub ' . app_path('User.php'));

        $this->updateStep();

        // 7. Enable last login feature

        exec('cp ' . $defaultFiles . 'Providers/EventServiceProvider.php.stub ' . app_path('Providers/EventServiceProvider.php'));

        $this->updateStep();

        // 8. Split routes

        exec('cp ' . $defaultFiles . 'Routes/api.php ' . base_path('routes/api.php'));
        exec('cp ' . $defaultFiles . 'Routes/auth.php ' . base_path('routes/auth.php'));
        exec('cp ' . $defaultFiles . 'Routes/channels.php ' . base_path('routes/channels.php'));
        exec('cp ' . $defaultFiles . 'Routes/console.php ' . base_path('routes/console.php'));
        exec('cp ' . $defaultFiles . 'Routes/web.php ' . base_path('routes/web.php'));

        exec('cp ' . $defaultFiles . 'Controllers/AppController.php.stub ' . app_path('Http/Controllers/AppController.php'));

        if (file_exists(resource_path('views/welcome.blade.php'))) {
            exec('unlink ' . resource_path('views/welcome.blade.php'));
        }

        exec('cp ' . $defaultFiles . 'Resources/views/index.blade.php.stub ' . resource_path('views/index.blade.php'));

        exec('cp ' . $defaultFiles . 'Providers/RouteServiceProvider.php.stub ' . app_path('Providers/RouteServiceProvider.php'));

        $this->updateStep();

        // 9. Update authentication

        exec('cp ' . $defaultFiles . 'Controllers/UsersController.php.stub ' . app_path('Http/Controllers/UsersController.php'));

        exec('cp ' . $defaultFiles . 'Controllers/Auth/ConfirmPasswordController.php.stub ' . app_path('Http/Controllers/Auth/ConfirmPasswordController.php'));
        exec('cp ' . $defaultFiles . 'Controllers/Auth/ForgotPasswordController.php.stub ' . app_path('Http/Controllers/Auth/ForgotPasswordController.php'));
        exec('cp ' . $defaultFiles . 'Controllers/Auth/LoginController.php.stub ' . app_path('Http/Controllers/Auth/LoginController.php'));
        exec('cp ' . $defaultFiles . 'Controllers/Auth/RegisterController.php.stub ' . app_path('Http/Controllers/Auth/RegisterController.php'));
        exec('cp ' . $defaultFiles . 'Controllers/Auth/ResetPasswordController.php.stub ' . app_path('Http/Controllers/Auth/ResetPasswordController.php'));
        exec('cp ' . $defaultFiles . 'Controllers/Auth/SocialAuthController.php.stub ' . app_path('Http/Controllers/Auth/SocialAuthController.php'));
        exec('cp ' . $defaultFiles . 'Controllers/Auth/VerificationController.php.stub ' . app_path('Http/Controllers/Auth/VerificationController.php'));

        exec('cp -R ' . $defaultFiles . 'Resources/lang ' . resource_path());

        $this->updateStep();

        // 10. Setup colortools

        $this->artisan('vendor:publish --tag=colortools');
        $this->artisan('colortools:setup');

        exec('cp ' . $defaultFiles . 'Models/File.php.stub ' . app_path('File.php'));

        $this->updateStep();

        // 11. Setup filetools

        // Publishing it with tags because namespacing is wrong.
        $this->artisan('vendor:publish --tag=filetools');
        $this->artisan('filetools:setup');

        exec('cp ' . $defaultFiles . 'Models/ImageStore.php.stub ' . app_path('ImageStore.php'));

        $this->updateStep();

        // 12. Setup wai emails

        $this->publishPackage('Wai\Emails\EmailsServiceProvider');

        $this->updateStep();

        // 13. Setup debug bar

        $this->publishPackage('Barryvdh\Debugbar\ServiceProvider');

        $this->updateStep();

        // 14. Setup htmlmin

        $this->publishPackage('HTMLMin\HTMLMin\HTMLMinServiceProvider');

        $this->updateStep();

        $this->fixPerms();

        $this->info('Setup finished');
    }
}

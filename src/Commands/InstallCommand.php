<?php

namespace Wai\Adminify\Commands;

class InstallCommand extends BaseCommand
{
    protected $signature = 'adminify:install';

    protected $description = 'Integrates vuetify admin panel with base Laravel installation';

    protected $step = 0;
    protected $totalSteps = 8;

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $this->info('Starting install');

        $defaultFiles = __DIR__ . '/../../stubs/';

//        1. Basic setup of adminify
        $this->artisan('adminify:setup');

        $this->updateStep();

//        2. Update Controllers

        exec('cp ' . $defaultFiles . 'Controllers/Adminify-AppController.php.stub ' . app_path('Http/Controllers/AppController.php'));
        exec('cp ' . $defaultFiles . 'Controllers/ResourcesController.php.stub ' . app_path('Http/Controllers/ResourcesController.php'));
        exec('cp ' . $defaultFiles . 'Controllers/FilesController.php.stub ' . app_path('Http/Controllers/FilesController.php'));
        exec('cp ' . $defaultFiles . 'Controllers/ImagesController.php.stub ' . app_path('Http/Controllers/ImagesController.php'));
        exec('cp ' . $defaultFiles . 'Controllers/LogsController.php.stub ' . app_path('Http/Controllers/LogsController.php'));
        exec('cp ' . $defaultFiles . 'Controllers/SettingsController.php.stub ' . app_path('Http/Controllers/SettingsController.php'));
        exec('cp ' . $defaultFiles . 'Controllers/Adminify-UsersController.php.stub ' . app_path('Http/Controllers/UsersController.php'));

        $this->updateStep();

//        3. Update Middleware

        exec('cp ' . $defaultFiles . 'Middleware/RedirectIfAuthenticated.php.stub ' . app_path('Http/Middleware/RedirectIfAuthenticated.php'));
        exec('cp ' . $defaultFiles . 'Middleware/VerifyCsrfToken.php.stub ' . app_path('Http/Middleware/VerifyCsrfToken.php'));

        $this->updateStep();

//        4. Update migrations + seeders + models

        exec('cp ' . $defaultFiles . 'Database/migrations/_200000_create_roles_table.php.stub ' . database_path('migrations/' . date('Y_m_d') . '_200000_create_roles_table.php'));
        exec('cp ' . $defaultFiles . 'Database/migrations/_300000_create_user_role_pivot.php.stub ' . database_path('migrations/' . date('Y_m_d') . '_300000_create_user_role_pivot.php'));
        exec('cp ' . $defaultFiles . 'Database/migrations/_400000_create_countries_table.php.stub ' . database_path('migrations/' . date('Y_m_d') . '_400000_create_countries_table.php'));
        exec('cp ' . $defaultFiles . 'Database/migrations/_500000_create_settings_table.php.stub ' . database_path('migrations/' . date('Y_m_d') . '_500000_create_settings_table.php'));
        exec('cp ' . $defaultFiles . 'Database/migrations/_600000_create_logs_table.php.stub ' . database_path('migrations/' . date('Y_m_d') . '_600000_create_logs_table.php'));
        exec('cp ' . $defaultFiles . 'Database/migrations/_700000_create_notifications_table.php.stub ' . database_path('migrations/' . date('Y_m_d') . '_700000_create_notifications_table.php'));

        exec('rm -R ' . database_path('seeds'));
        $this->copyFolderContents($defaultFiles . 'Database/seeds', database_path('seeds'));

        exec('cp ' . $defaultFiles . 'Models/Adminify-User.php.stub ' . app_path('Setting.php'));
        exec('cp ' . $defaultFiles . 'Models/Country.php.stub ' . app_path('Country.php'));
        exec('cp ' . $defaultFiles . 'Models/Log.php.stub ' . app_path('Log.php'));
        exec('cp ' . $defaultFiles . 'Models/Notification.php.stub ' . app_path('Notification.php'));
        exec('cp ' . $defaultFiles . 'Models/Role.php.stub ' . app_path('Role.php'));
        exec('cp ' . $defaultFiles . 'Models/Setting.php.stub ' . app_path('Setting.php'));

        $this->updateStep();

//        5. Update additional files

        exec('cp ' . $defaultFiles . 'Routes/adminify-channels.php ' . base_path('routes/channels.php'));
        exec('cp ' . $defaultFiles . 'Routes/adminify-web.php ' . base_path('routes/web.php'));


        exec('cp -R ' . $defaultFiles . 'Public ' . base_path('public'));
        exec('tar -zxvf ' . public_path('static/videos.tar.gz') . ' --directory ' . public_path('static'));
        exec('rm ' . public_path('static/videos.tar.gz'));

        exec('cp ' . $defaultFiles . 'Config/services.php.stub ' . config_path('services.php'));

        $this->updateStep();

//        6. Install required NPM packages

        exec('cp ' . $defaultFiles . 'package.json ' . base_path());

        $devPackagesToBeAdded = [
            'cross-env',
            'laravel-mix',
            'resolve-url-loader',
            'sass@1.22.0',
            'sass-loader@7.1.0',
            'style-loader'
        ];

        $packagesToBeAdded = [
            '@mdi/js',
            'axios',
            'adminify',
            'domurl',
            'cropperjs',
            'js-file-download',
            'moment',
            'highcharts',
            'highcharts-vue',
            'lodash',
            'vue',
            'vuedraggable',
            'vue-infinite-loading',
            'vue-router',
            'vue-toasted',
            'vue-videobg',
            'vue-template-compiler',
            'vuetify',
            'vuetify-loader',
            'vuex'
        ];

        exec('npm install --save-dev ' . implode(' ', $devPackagesToBeAdded));
        exec('npm install --save ' . implode(' ', $packagesToBeAdded));

        $this->updateStep();

//        7. Publish needed composer providers

        $this->publishPackage('Barryvdh\Debugbar\ServiceProvider');
        $this->publishPackage('Spatie\Cors\CorsServiceProvider');
        $this->publishPackage('GeoSot\EnvEditor\ServiceProvider');

        exec('cp ' . $defaultFiles . 'Config/avatar.php.stub ' . config_path('avatar.php'));
        exec('cp -R ' . $defaultFiles . 'Storage ' . base_path('storage'));
        exec('cp ' . $defaultFiles . 'gitignoredir.stub ' . storage_path('env-editor/.gitignore'));

        $this->updateStep();

//        8. Setup resources and compiler

        exec('cp ' . $defaultFiles . 'Resources/views/adminify-index.blade.php.stub ' . resource_path('views/index.blade.php'));
        exec('cp ' . $defaultFiles . 'Resources/views/adminify-any.blade.php.stub ' . resource_path('views/any.blade.php'));

        exec('cp -R ' . $defaultFiles . 'Resources/sass/* ' . resource_path('sass'));

        exec('cp ' . $defaultFiles . 'webpack.mix.js ' . base_path());

        $this->updateStep();

        $this->fixPerms();

        $this->info('Install finished');
    }

    private function copyFolderContents($filesPath, $newPath)
    {
        if (!is_dir($newPath)) {
            mkdir($newPath, 0777, true);
        }

        foreach (glob($filesPath . '/*.stub') as $filePath) {
            $fileName = substr($filePath, strrpos($filePath, '/') + 1);
            $newFileName = str_replace('.stub', '', $fileName);

            exec('cp ' . $filePath . ' ' . $newPath . '/' . $newFileName);
        }
    }

}

<?php

namespace Wai\Adminify\Commands;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use League\Flysystem\MountManager;
use Illuminate\Support\ServiceProvider;
use League\Flysystem\Filesystem as Flysystem;
use League\Flysystem\Adapter\Local as LocalAdapter;

class BaseCommand extends Command
{
    protected $files;

    public function __construct()
    {
        parent::__construct();

        $this->files = new Filesystem();
    }

    public function fixPerms()
    {
        $this->info('Setting permissions...');
        exec('chmod -R ugo+wr .');
        $this->info('Fixing ownership...');
        exec('chown -R 33:33 .');
        $this->info('Done!');
    }

    public function updateStep()
    {
        $this->step++;
        $this->warn($this->step . '/' . $this->totalSteps . ' - ' . round(($this->step / $this->totalSteps) * 100, 2) . '%');
    }

    public function publishPackage($provider, $tag = null)
    {
        $files = ServiceProvider::pathsToPublish($provider, $tag);

        foreach ($files as $from => $to) {
            $this->publishItem($from, $to);
        }
    }

    public function publishItem($from, $to)
    {
        if ($this->files->isFile($from)) {
            return $this->publishFile($from, $to);
        } elseif ($this->files->isDirectory($from)) {
            return $this->publishDirectory($from, $to);
        }

        $this->error("Can't locate path: <{$from}>");
    }

    protected function publishFile($from, $to)
    {
        $this->createParentDirectory(dirname($to));

        $this->files->copy($from, $to);

        $this->status($from, $to, 'File');
    }

    protected function publishDirectory($from, $to)
    {
        $this->moveManagedFiles(new MountManager([
            'from' => new Flysystem(new LocalAdapter($from)),
            'to' => new Flysystem(new LocalAdapter($to)),
        ]));

        $this->status($from, $to, 'Directory');
    }

    protected function createParentDirectory($directory)
    {
        if (!$this->files->isDirectory($directory)) {
            $this->files->makeDirectory($directory, 0755, true);
        }
    }

    protected function status($from, $to, $type)
    {
        $from = str_replace(base_path(), '', realpath($from));

        $to = str_replace(base_path(), '', realpath($to));

        $this->line('<info>Copied ' . $type . '</info> <comment>[' . $from . ']</comment> <info>To</info> <comment>[' . $to . ']</comment>');
    }

    protected function moveManagedFiles($manager)
    {
        foreach ($manager->listContents('from://', true) as $file) {
            if ($file['type'] === 'file' && (!$manager->has('to://' . $file['path']))) {
                $manager->put('to://' . $file['path'], $manager->read('from://' . $file['path']));
            }
        }
    }

    public function artisan($cmd)
    {
        exec('/usr/bin/php ' . base_path() . '/artisan ' . $cmd);
    }
}

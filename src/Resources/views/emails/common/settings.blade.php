<?php
if (class_exists('App\Setting')) {
    $mailConfiguration = \App\Setting::getMailConfiguration();

    foreach ($mailConfiguration as $key => $value) {
        if ($key == 'company') {
            $settings->setCompanyDetails($value);
        } else {
            $settings->{$key} = $value;
        }
    }
}
?>

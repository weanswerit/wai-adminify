@include('adminify::emails.common.settings')

@extends('emails::common.master')

@section('content')
    @yield('content')
@overwrite

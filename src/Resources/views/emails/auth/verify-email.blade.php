@extends('adminify::emails.common.master')

@section('content')

    @include('emails::sections.spacer', ['height' => '80'])

    @include('emails::sections.text_button', [
        'heading' => 'Hi there,',
        'headingTextAlign' => 'left',
        'text' => 'You are receiving this email to verify your email address for your account.',
        'buttonUrl' => $url,
        'buttonText' => 'Verify Email',
        'buttonAlign' => 'left'
    ])

    @include('emails::sections.text_block', [
        'text' => 'If you did not request a email verification link, no further action is required.<br><br> Regards, <br> '. config('app.name'),
        'padding' => '0px 40px 40px'
    ])

    @include('emails::sections.hr', [
        'padding' => '0 20px'
    ])

    @include('emails::sections.text_block', [
        'text' => 'If you’re having trouble clicking the "Verify Email" button, copy and paste the url below into your web browser: '.
                \Wai\Emails\Email::link($url, $url),
    ])

    @include('emails::sections.spacer', ['height' => '80'])

@endsection

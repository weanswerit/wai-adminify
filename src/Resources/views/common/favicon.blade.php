@foreach(\App\Setting::getFaviconUrls() as $type => $urls)
    @foreach($urls as $url)
        @if($type == 'apple')
            <link
                rel="apple-touch-icon"
                sizes="{{ $url['size'] }}x{{ $url['size'] }}"
                href="{{ $url['src'] }}"
            >
        @elseif($type == 'android')
            <link
                rel="icon"
                type="{{ $url['type'] }}"
                sizes="{{ $url['size'] }}x{{ $url['size'] }}"
                href="{{ $url['src'] }}"
            >
        @elseif($type == 'microsoft')
            <meta
                name="msapplication-TileImage"
                content="{{ $url['src'] }}"
            >
        @endif
    @endforeach
@endforeach


<link
    rel="manifest"
    href="{{ route('favicon_manifest') }}"
>
<meta
    name="msapplication-TileColor"
    content="#ffffff"
>
<meta
    name="theme-color"
    content="#ffffff"
>

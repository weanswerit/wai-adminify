<!DOCTYPE html>
<html lang="en" data-overflow-hidden>
<head>
    @include('adminify::common.header')
</head>
<body
    @if(config('auth.register')) data-reg="true" @endif
    @if(config('auth.social_login')) data-google="true" @endif
    data-name="{{ config('app.name') }}"
>

@include('adminify::common.preloader')

@if( $__env->yieldContent('body'))
    @yield('body')
@endif

@include('adminify::common.footer')
</body>
</html>

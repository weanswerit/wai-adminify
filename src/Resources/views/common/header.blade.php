<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="csrf-token" content="{{ csrf_token() }}">

@if($__env->yieldContent('title'))
    <title>@yield('title')</title>
@else
    <title>{{ config('app.name') }}</title>
@endif

<link rel="stylesheet" href="{{ mix('css/pre-loader.css') }}" type="text/css">
<link rel="stylesheet" href="{{ mix('css/app.css') }}" type="text/css">

@include('adminify::common.favicon')

@if(session('redirect'))
    <meta name="redirect-url" content="{{ session('redirect') }}">
@endif

<script>
    window.app = {!! json_encode([
        'roles' => \App\Role::query()->orderBy('id', 'asc')->get(['id', 'name']),
    ]) !!};
</script>

<?php

namespace Wai\Adminify\Jobs;

use App\User;
use Wai\Emails\Email;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class MailUserPasswordReset implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $token;
    private $email;

    /**
     * Create a new job instance.
     *
     * @param $email
     * @param $token
     */
    public function __construct($email, $token)
    {
        $this->token = $token;
        $this->email = $email;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \Exception
     */
    public function handle()
    {
        $email = $this->email;
        $token = $this->token;

        $user = User::findByEmail($email);

        $view = 'emails.auth.reset-password';

        if (!view()->exists($view)) {
            $view = 'adminify::' . $view;
        }

        $mail = new Email($view);

        $from['name'] = config('mail.from.name');
        $from['email'] = config('mail.from.address');

        $mail->send(['url' => $this->passwordResetUrl($user, $token)], function ($m) use ($user, $from) {
            $m->to($user->email, $user->name);
            $m->bcc(config('app.support_email'), config('app.name'));
            $m->replyTo($from['email'], $from['name']);
            $m->subject('Reset your password');
        });
    }

    protected function passwordResetUrl($user, $token)
    {
        return route('password.reset', [
            'token' => $token,
            'email' => base64_encode($user->email)
        ]);
    }
}

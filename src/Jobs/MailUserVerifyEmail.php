<?php

namespace Wai\Adminify\Jobs;

use App\User;
use Wai\Emails\Email;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\URL;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class MailUserVerifyEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $token;
    private $email;

    /**
     * Create a new job instance.
     *
     * @param $email
     */
    public function __construct($email)
    {
        $this->email = $email;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \Exception
     */
    public function handle()
    {
        $email = $this->email;

        $user = User::findByEmail($email);

        $view = 'emails.auth.verify-email';

        if (!view()->exists($view)) {
            $view = 'adminify::' . $view;
        }

        $mail = new Email($view);

        $from['name'] = config('mail.from.name');
        $from['email'] = config('mail.from.address');

        $mail->send(['url' => $this->verificationUrl($user)], function ($m) use ($user, $from) {
            $m->to($user->email, $user->name);
            $m->bcc(config('app.support_email'), config('app.name'));
            $m->replyTo($from['email'], $from['name']);
            $m->subject('Verify your email');
        });
    }

    protected function verificationUrl($user)
    {
        return URL::temporarySignedRoute(
            'verification.verify',
            Carbon::now()->addMinutes(config('auth.verification.expire', 60)),
            [
                'id' => $user->id,
                'hash' => sha1($user->email),
            ]
        );
    }
}

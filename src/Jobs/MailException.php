<?php

namespace Wai\Adminify\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class MailException implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $exceptionHtml;

    /**
     * Create a new job instance.
     *
     * @param string $exceptionHtml
     */
    public function __construct(string $exceptionHtml)
    {
        $this->exceptionHtml = $exceptionHtml;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        \Mail::send('adminify::emails.exception.html', ['html' => $this->exceptionHtml], function ($m) {
            $m->to(config('app.support_email'), config('app.name'));
            $m->subject(config('app.name') . ' - Exception');
        });
    }
}

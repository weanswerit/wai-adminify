<?php

namespace Wai\Adminify\Controllers;

use App\ImageStore;

class ImagesController extends VueController
{
    protected $class = ImageStore::class;
    protected $itemName = 'image';

    protected $orderAsc = false;
    protected $orderBy = 'created_at';
    protected $itemsPerPage = 18;

    protected $singleAppends = [];
    protected $multipleAppends = [];

    protected $singleRelationships = [];
    protected $multipleRelationships = [];

    public function searchQuery()
    {
        $searchQuery = request('searchQuery');

        $results = $this->class::with($this->multipleRelationships);

        if (isset($searchQuery) and !empty($searchQuery)) {
            $searchQuery = '%' . $searchQuery . '%';

            $results->where(function ($query) use ($searchQuery) {
                $query->where('name', 'LIKE', $searchQuery);
                $query->orWhere('metadata', 'LIKE', $searchQuery);
            });
        }

        $this->checkSortBy($results);

        return $results;
    }
}

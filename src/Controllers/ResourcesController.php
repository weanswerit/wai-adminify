<?php

namespace Wai\Adminify\Controllers;

use App\Log;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Str;

class ResourcesController extends VueController
{
    /**
     * Returns only id and name of all roles in database.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getRoles()
    {
        return response()->json([
            $this->getResponseKey(__FUNCTION__) => self::getRaw($this->getResponseModel(__FUNCTION__), [
                'id',
                'name',
            ]),
        ]);
    }

    /**
     * Returns iso, iso3, country name, and prefix of all countries in database.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCountries()
    {
        return response()->json([
            $this->getResponseKey(__FUNCTION__) => \DB::table('countries')->get()
        ]);
    }

    /**
     * Returns all users in database.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUsers()
    {
        return response()->json(
            User::query()
                ->find(\Auth::user()->id)
                ->getUserResources(request('timestamp', 0))
        );
    }

    /**
     * Returns log types.
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \ReflectionException
     */
    public function getLogTypes()
    {
        $logs = [];

        $logClass = new \ReflectionClass(Log::class);

        foreach ($logClass->getConstants() as $logName => $logId) {
            if ($logId == 'created_at' || $logId == 'updated_at') {
                continue;
            }

            $logs[] = [
                'id' => $logId,
                'name' => Log::getLabel($logId)
            ];
        }

        return response()->json([
            'logTypes' => $logs
        ]);
    }

    public function getBirthdays()
    {
        $birthdays = User::getUpcomingBirthdays(8);

        $userBirthdays = [];

        foreach ($birthdays as $userId => $date) {
            $user = User::query()->find($userId)->append(['thumbnail']);
            $user->birthday = Carbon::parse($date)->format('j M');
            $userBirthdays[] = $user;
        }

        return response()->json([
            'birthdays' => $userBirthdays,
        ]);
    }

    /**
     * Make a direct query to database to fetch specific columns and ignores model.
     *
     * @param $model
     * @param array $columns
     * @param bool $deleted
     * @return mixed
     */
    public static function getRaw($model, $columns = [], $deleted = false)
    {
        $query = \DB::table((new $model)->getTable());

        if ($deleted) {
            $query->where('deleted_at', '=', null);
        }

        return $query->get($columns);
    }

    /**
     * If item name is set in parent function will return item name as response key.
     * Otherwise determines item name from function name.
     *
     * @param $functionName
     * @return mixed|null
     */
    public function getResponseKey($functionName)
    {
        if (is_null($this->itemName)) {
            $this->itemName = lcfirst(str_replace('get', '', $functionName));
        }

        return $this->itemName;
    }

    /**
     * If class/model is set in parent function will return class/model as response model.
     * Otherwise determines class/model from function name.
     *
     * @param $functionName
     * @return null|string
     */
    public function getResponseModel($functionName)
    {
        if (is_null($this->class)) {
            $class = ucfirst(Str::singular(str_replace('get', '', strtolower($functionName))));
            $this->class = '\App\\' . $class;
        }

        return $this->class;
    }
}

<?php

namespace Wai\Adminify\Controllers;

use App\File;

class FilesController extends VueController
{
    protected $class = File::class;
    protected $itemName = 'file';

    protected $orderAsc = false;
    protected $orderBy = 'created_at';
    protected $itemsPerPage = 15;

    protected $singleAppends = [];
    protected $multipleAppends = [];

    protected $singleRelationships = [];
    protected $multipleRelationships = [];

    public function searchQuery()
    {
        $searchQuery = request('searchQuery');

        $results = $this->class::where('hidden', 0)->with($this->multipleRelationships);

        if (isset($searchQuery) and !empty($searchQuery)) {
            $searchQuery = '%' . $searchQuery . '%';

            $results->where(function ($query) use ($searchQuery) {
                $query->where('name', 'LIKE', $searchQuery);
                $query->orWhere('metadata', 'LIKE', $searchQuery);
            });
        }

        $this->checkSortBy($results);

        return $results;
    }

    public function associations()
    {
        $results = \DB::table('file_associations')->where('file_id', request('id'));

        return response()->json([
            __FUNCTION__ => $results->paginate(request('itemsPerPage', 25)),
        ]);
    }
}

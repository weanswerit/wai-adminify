<?php

namespace Wai\Adminify\Controllers;

use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

class UsersController extends VueController
{
    use SendsPasswordResetEmails;

    /**
     * Returns the currently logged in user with relationships and appends.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUser()
    {
        return response()->json([
            $this->itemName => $this->class::with($this->singleRelationships)
                ->find(\Auth::id())
                ->append(array_merge($this->singleAppends, ['impersonator']))
        ]);
    }

    public function passwordChange()
    {
        $this->broker()->sendResetLink(['email' => request('email')]);
    }

    public function impersonateLogin($userId)
    {
        $this->class::impersonateLogin($userId);
        return redirect('/');
    }

    public function impersonateReturn()
    {
        $this->class::impersonateReturn();
        return redirect('/');
    }
}

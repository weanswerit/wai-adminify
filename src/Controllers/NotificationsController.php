<?php

namespace Wai\Adminify\Controllers;

use App\Notification;
use Wai\Adminify\Events\UpdateNotifications;

class NotificationsController extends VueController
{
    public function count()
    {
        return response()->json([
            'count' => \Auth::user()
                ->notifications()
                ->whereNull('read_at')
                ->count()
        ]);
    }

    public function get()
    {
        $offset = request('offset');
        $amount = request('amount');

        return response()->json([
            'notifications' => $this->processNotifications($offset, $amount),
            'count' => \Auth::user()->notifications()->count()
        ]);
    }

    public function dismiss()
    {
        $notification = \Auth::user()
            ->notifications()
            ->where('id', request('id'))
            ->first();

        if ($notification) {
            $notification->delete();
        }
    }

    public function open()
    {
        $notification = \Auth::user()
            ->notifications()
            ->where('id', request('id'))
            ->first();

        if ($notification) {
            if (isset($notification->data['consume']) and !empty($notification->data['consume'])) {
                $notification->delete();
            }

            if (isset($notification->data['groupConsume']) and !empty($notification->data['groupConsume'])) {
                $otherNotifications = Notification::where('identifier', $notification->identifier)
                    ->where('id', '!=', $notification->id)
                    ->get();

                foreach ($otherNotifications as $groupNotification) {
                    $groupUserId = $groupNotification->notifiable_id;
                    $groupNotification->delete();
                    event(new UpdateNotifications($groupUserId));
                }
            }
        }
    }

    public function clear()
    {
        \Auth::user()->notifications()->delete();
    }

    private function processNotifications($offset, $amount = 10)
    {
        $notifications = [];

        $now = date('Y-m-d H:i:s');

        $userNotifications = \Auth::user()
            ->notifications()
            ->skip($offset * $amount)
            ->take($amount)
            ->get();

        foreach ($userNotifications as $notification) {
            if (is_null($notification->getObject())) {
                $notification->delete();
                continue;
            }
            if (is_null($notification->read_at)) {
                $notification->read_at = $now;
                $notification->save();
            }
            $notifications[] = $notification->getJson();
        }

        return $notifications;
    }
}

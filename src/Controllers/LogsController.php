<?php

namespace Wai\Adminify\Controllers;

use App\Log;
use App\Role;
use Carbon\Carbon;
use Illuminate\Support\Str;

class LogsController extends VueController
{
    protected $class = Log::class;
    protected $itemName = 'log';

    protected $orderAsc = false;
    protected $orderBy = 'created_at';
    protected $itemsPerPage = 15;

    protected $singleAppends = [];
    protected $multipleAppends = ['user_name', 'description'];

    protected $singleRelationships = [];
    protected $multipleRelationships = ['user', 'association'];

    /**
     * Custom search query for activities.
     * Searches through name.
     *
     * @return mixed
     */
    public function customSearchPaginated()
    {
        if (!empty($this->multipleAppends)) {
            $this->class::setStaticAppends($this->multipleAppends);
        }

        $filter = request('filter');

        if (isset($filter['startDate']) and !empty($filter['startDate'])) {
            $startDate = $filter['startDate'];
        } else {
            $startDate = Carbon::today()->subDays(7)->format('Y-m-d');
        }

        if (isset($filter['endDate']) and !empty($filter['endDate'])) {
            $endDate = $filter['endDate'];
        } else {
            $endDate = Carbon::today()->format('Y-m-d');
        }

        $totalDays = Carbon::parse($endDate)->diffInDays(Carbon::parse($startDate));

        $query = $this->searchQuery();
        $stats['count'] = $query->count();
        $stats['per_day'] = ceil($stats['count'] / $totalDays);
        $stats['users'] = $this->searchQuery()->distinct('user_id')->count('user_id');

        return response()->json([
            Str::plural($this->itemName) => $this->getSearchQuery()->paginate($this->itemsPerPage),
            'stats' => $stats
        ]);
    }

    /**
     * Custom search query for users.
     *
     * @param bool $trashedOnly
     * @return mixed
     */
    public function searchQuery($trashedOnly = false)
    {
        $filter = request('filter');
        $itemsPerPage = request('itemsPerPage');

        if ($itemsPerPage) {
            $this->itemsPerPage = $itemsPerPage;
        }

        $results = $this->class::query();

        if ($trashedOnly) {
            $results->onlyTrashed();
        }

        $results->with($this->multipleRelationships);


        if (isset($filter['startDate']) and !empty($filter['startDate'])) {
            $results->where('created_at', '>=', $filter['startDate']);
        }

        if (isset($filter['endDate']) and !empty($filter['endDate'])) {
            $results->where('created_at', '<=', $filter['endDate']);
        }

        if (isset($filter['ids']) and !empty($filter['ids'])) {
            $userIds = $filter['ids'];
            $results->where(function ($query) use ($userIds) {
                $query->whereIn('user_id', $userIds);
            });
        } else {
            $user = \Auth::user();
            if (!(
                $user->hasRole(Role::ADMIN)
                or $user->hasRole(Role::SUPER_ADMIN)
            )) {
                $results->where(function ($query) use ($user) {
                    $query->where('user_id', $user->id);
                });
            }
        }

        if (isset($filter['searchQuery']) and !empty($filter['searchQuery'])) {
            $this->searchColumnsForText($results, $filter['searchQuery']);
        }

        if (isset($filter['filters']) and !empty($filter['filters'])) {
            $this->applyFilters($results, $filter['filters']);
        }

        $this->checkSortBy($results);

        return $results;
    }

    public function download()
    {
        $query = $this->searchQuery()->with([]);

        $query->take(20);

        return \App\Log::exportCollectionForExcel($query->get());
    }

    /**
     * Specify which table columns to search for text and return result query.
     *
     * @param $results
     * @param $searchQuery
     * @return mixed
     */
    public function searchColumnsForText($results, $searchQuery)
    {
        $results = $results->filter(function ($q) use ($searchQuery) {
            return (stristr($q->action_label, $searchQuery) !== false || stristr($q->user_name, $searchQuery) !== false);
        });

        return $results;
    }

    /**
     * Specify how to build search query with filter keys and return result query.
     *
     * @param $query
     * @param $filters
     * @return mixed
     */
    public function applyFilters($query, $filters)
    {
        foreach ($filters as $key => $ids) {
            if (empty($ids)) {
                continue;
            }
            switch ($key) {
                case 'types':
                    $query->whereIn('type', $ids);
                    break;
                default:
                    break;
            }
        }

        return $query;
    }

    /**
     * If request has sort by key which is sent with vuetify table
     * sorting then we will order the requested column name by
     * descending or ascending.
     *
     * @param $results
     * @return mixed
     */
    public function checkSortBy($results)
    {
        if (request('sortBy', null)) {
            $sortDesc = request('sortDesc');
            foreach (request('sortBy') as $key => $value) {
                if (!isset($sortDesc[$key])) $sortDesc[$key] = false;

                if ($sortDesc[$key]) {
                    $results = $results->sortByDesc($value);
                } else {
                    $results = $results->sortBy($value);
                }
            }
        }

        return $results;
    }

}

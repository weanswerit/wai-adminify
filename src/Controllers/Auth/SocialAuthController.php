<?php

namespace Wai\Adminify\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Laravel\Socialite\Facades\Socialite;

class SocialAuthController extends Controller
{
    const PROVIDER_GOOGLE = 1;

    public function loginGoogle()
    {
        return Socialite::driver('google')->redirect();
    }

    public function callbackGoogle()
    {
        $providerUser = Socialite::driver('google')->user();

        $user = User::findByEmail($providerUser->getEmail());

        if (!$user) {
            return redirect()->route('user.not-found', ['email' => base64_encode($providerUser->getEmail())]);
        }

        \Auth::login($user, true);

        return redirect()->route('home');
    }

}

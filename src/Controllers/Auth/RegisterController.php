<?php

namespace Wai\Adminify\Controllers\Auth;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Auth\Events\Registered;
use App\Http\Controllers\UsersController;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends UsersController
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        if (config('auth.register')) {
            $view = 'auth.register';

            if (!view()->exists($view)) {
                $view = 'adminify::' . $view;
            }

            return view($view);
        } else {
            return redirect()->route('login');
        }
    }

    /**
     * Handle a registration request for the application.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        if (!config('auth.register')) {
            abort(401, 'Not authorized!');
        }

        $this->validator($request->all())->validate();

        event(new Registered($user = $this->createUser($request->all())));

        $this->guard()->login($user);

        return $this->registered($request, $user)
            ?: redirect($this->redirectPath());
    }

    /**
     * The user has been registered.
     *
     * @param \Illuminate\Http\Request $request
     * @param mixed $user
     * @return mixed
     */
    protected function registered(Request $request, $user)
    {
        if (config('auth.email_verification')) {
            $user->sendEmailVerificationNotification();
        }

        if ($request->expectsJson()) {
            if (config('auth.email_verification')) {
                return response()->json(
                    array_merge(['verified' => 'false'], $this->getAuthenticatedUser())
                );
            } else {
                return response()->json($this->getAuthenticatedUser());
            }
        }
    }

    /**
     * Returns authenticated information with required appends and relationships
     * from User Controller.
     *
     * @return array
     */
    protected function getAuthenticatedUser()
    {
        return [
            $this->itemName => $this->class::with($this->singleRelationships)
                ->find(\Auth::id())
                ->append($this->singleAppends)
        ];
    }

    protected function redirectTo()
    {
        if (config('auth.email_verification')) {
            return route('verification.notice');
        } else {
            return $this->redirectTo;
        }
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     * @return \App\User
     */
    protected function createUser(array $data)
    {
        if (config('auth.register')) {
            return User::create([
                'first_name' => $data['first_name'],
                'last_name' => $data['last_name'],
                'email' => $data['email'],
                'password' => $data['password'],
                'details' => [],
            ]);
        }
    }
}

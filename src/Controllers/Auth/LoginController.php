<?php

namespace Wai\Adminify\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use App\Http\Controllers\UsersController;
use Illuminate\Validation\ValidationException;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends UsersController
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        $view = 'auth.login';

        if (!view()->exists($view)) {
            $view = 'adminify::' . $view;
        }

        return view($view);
    }

    /**
     * Send the response after the user was authenticated.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    protected function sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();

        $this->clearLoginAttempts($request);

        if (config('auth.email_verification') and \Auth::user()->email_verified_at == null) {
            return response()->json(array_merge(['verified' => 'false'], $this->getAuthenticatedUser()));
        } else {
            return response()->json($this->getAuthenticatedUser());
        }
    }

    /**
     * Returns authenticated information with required appends and relationships
     * from User Controller.
     *
     * @return array
     */
    protected function getAuthenticatedUser()
    {
        return [
            $this->itemName => $this->class::with($this->singleRelationships)
                ->find(\Auth::id())
                ->append(array_merge($this->singleAppends, ['impersonator']))
        ];
    }

    /**
     * Redirect the user after determining they are locked out.
     *
     * @param \Illuminate\Http\Request $request
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function sendLockoutResponse(Request $request)
    {
        $seconds = $this->limiter()->availableIn(
            $this->throttleKey($request)
        );

        if ($request->expectsJson()) {
            return response()->json(
                [
                    'message' => Lang::get('auth.throttle', [
                        'seconds' => $seconds,
                        'minutes' => ceil($seconds / 60),
                    ])
                ], 429);
        }

        throw ValidationException::withMessages([
            $this->username() => [Lang::get('auth.throttle', [
                'seconds' => $seconds,
                'minutes' => ceil($seconds / 60),
            ])],
        ])->status(429);
    }

    /**
     * Get the failed login response instance.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function sendFailedLoginResponse(Request $request)
    {
        if ($request->expectsJson()) {
            return response()->json(
                [
                    'message' => trans('auth.failed')
                ], 422);
        }

        throw ValidationException::withMessages([
            $this->username() => [trans('auth.failed')],
        ]);
    }

    /**
     * Log the user out of the application.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        \Session::forget('impersonator_id');

        if ($request->expectsJson()) {
            return response()->json(['token' => csrf_token()]);
        } else {
            return redirect()->route('login');
        }
    }

    /**
     *  User is not found in database when trying to login via social media platform.
     *
     * @param null $email
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function userMissing($email = null)
    {
        if (view()->exists('auth.user-not-found')) {
            return view('auth.user-not-found');
        } else {
            return view('adminify::user-not-found');
        }
    }
}

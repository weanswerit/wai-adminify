<?php

namespace Wai\Adminify\Controllers\Auth;

use App\User;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Auth\Events\PasswordReset;
use App\Http\Controllers\UsersController;
use Illuminate\Foundation\Auth\ResetsPasswords;

class ResetPasswordController extends UsersController
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Display the password reset view for the given token.
     *
     * If no token is present, display the link request form.
     *
     * @param \Illuminate\Http\Request $request
     * @param string|null $token
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showResetForm(Request $request, $token = null)
    {
        $view = 'auth.passwords.reset';

        if (view()->exists($view)) {
            return view($view)->with(
                ['token' => $token, 'email' => $request->email]
            );
        } else {
            return view('adminify::' . $view);
        }
    }

    /**
     * Reset the given user's password.
     *
     * @param User $user
     * @param string $password
     * @return void
     */
    protected function resetPassword($user, $password)
    {
        $user->password = $password;

        $user->setRememberToken(Str::random(60));

        $user->save();

        event(new PasswordReset($user));

        $this->guard()->login($user);
    }

    /**
     * Get the response for a successful password reset.
     *
     * @param \Illuminate\Http\Request $request
     * @param string $response
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    protected function sendResetResponse(Request $request, $response)
    {
        if ($request->expectsJson()) {
            if (config('auth.email_verification') && \Auth::user()->email_verified_at == null) {
                return response()->json(
                    array_merge(['verified' => 'false'], $this->getAuthenticatedUser())
                );
            } else {
                return response()->json($this->getAuthenticatedUser());
            }
        }

        return redirect($this->redirectTo);
    }

    /**
     * Get the response for a failed password reset.
     *
     * @param \Illuminate\Http\Request $request
     * @param string $response
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    protected function sendResetFailedResponse(Request $request, $response)
    {
        if ($request->expectsJson()) {
            return response()->json(
                [
                    'message' => trans($response)
                ], 403);
        }

        return redirect()->back()
            ->withInput($request->only('email'))
            ->withErrors(['email' => trans($response)]);
    }

    /**
     * Returns authenticated information with required appends and relationships
     * from User Controller.
     *
     * @return array
     */
    protected function getAuthenticatedUser()
    {
        return [
            $this->itemName => $this->class::with($this->singleRelationships)
                ->find(\Auth::id())
                ->append($this->singleAppends)
        ];
    }
}

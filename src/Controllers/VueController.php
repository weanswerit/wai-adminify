<?php

namespace Wai\Adminify\Controllers;

use Illuminate\Support\Str;
use Illuminate\Support\Collection;
use Illuminate\Pagination\Paginator;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\LengthAwarePaginator;

class VueController extends Controller
{
    protected $class;
    protected $keyName = 'id';
    protected $itemName = 'item';

    protected $orderAsc = true;
    protected $orderBy = null;

    protected $itemsPerPage = 10;
    protected $logItemsPerPage = 10;

    protected $singleRelationships = [];
    protected $multipleRelationships = [];

    protected $singleAppends = [];
    protected $multipleAppends = [];

    protected $searchColumns = [];

    /**
     * Construct method
     *
     * Checks if action is method/action is available on parent controller.
     * If method or action does not exists a "Method not found" error is thrown.
     * If no method or action is specified then the server throws a 406 (Not Acceptable) error.
     *
     * @param null $action
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws \Exception
     */
    public function vueHelper($action = null)
    {
        if (is_null($action) and request()->has('action')) {
            $action = request()->get('action');
        }

        if ($action) {
            if (in_array($action, get_class_methods($this))) {
                return $this->$action();
            } else {
                throw new \Exception('Method not found');
            }
        } else {
            return response('Not acceptable', '406');
        }
    }

    /**
     * Inserts a record based on parent controller's model class requirements.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function create()
    {
        $item = request($this->itemName);
        $i = new $this->class;
        foreach ($item as $param => $value) {
            if (!in_array($param, $this->singleAppends)) {
                $i->$param = $value;
            }
        }
        $i->save();

        foreach ($item as $param => $value) {
            if (in_array($param, $this->singleAppends)) {
                $i->$param = $value;
            }
        }

        if (method_exists($this, 'afterCreate')) {
            $this->afterCreate($i);
        }

        return response()->json([
            $this->keyName => $i->{$this->keyName},
        ]);
    }

    /**
     * Retrieves a record based on parent controller's model class with
     * parent controller's single relationships and appends.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function get()
    {
        $hasSoftDeletes = in_array(\Illuminate\Database\Eloquent\SoftDeletes::class, class_uses($this->class));

        if ($hasSoftDeletes) {
            $i = $this->class::withTrashed()->with($this->singleRelationships);
        } else {
            $i = $this->class::with($this->singleRelationships);
        }

        $i = $i->find(request($this->keyName))
            ->append($this->singleAppends);

        if (method_exists($this, 'beforeGet')) {
            $this->beforeGet($i);
        }

        return response()->json([
            $this->itemName => $i,
        ]);
    }

    /**
     * Updates a record based on parent's controller model class.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update()
    {
        $item = request($this->itemName);

        $i = $this->class::with($this->singleRelationships)->find($item[$this->keyName]);

        $allowedParams = array_keys($i->getAttributes());

        foreach ($item as $param => $value) {
            if (in_array($param, $allowedParams) or in_array($param, $this->singleAppends)) {
                $i->$param = $value;
            }
        }

        $i->update();

        $changes = $i->getChanges();
        if ($i->timestamps and isset($changes['updated_at'])) {
            unset($changes['updated_at']);
        }

        $i->append($this->singleAppends);

        $i->load($this->singleRelationships);

        if (method_exists($this, 'afterUpdate')) {
            $this->afterUpdate($i, $changes);
        }

        return response()->json([
            $this->itemName => $i,
            'changes' => $changes,
        ]);
    }

    /**
     * Deletes a record based on parent's controller model class.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete()
    {
        $i = $this->class::query()->find(request($this->keyName));
        $i->delete();

        if (method_exists($this, 'afterDelete')) {
            $this->afterDelete($i);
        }

        return response()->json([
            'message' => ucfirst($this->itemName) . ' deleted',
        ]);
    }

    /**
     * Retrieves all the records of parent's controller model class.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAll()
    {
        if (!empty($this->multipleAppends)) {
            $this->class::setStaticAppends($this->multipleAppends);
        }

        if (method_exists($this, 'beforeGetMultiple')) {
            $this->beforeGetMultiple();
        }

        return response()->json([
            Str::plural($this->itemName) => $this->class::with($this->multipleRelationships)->get(),
        ]);
    }

    /**
     * Returns paginated results for all records of parent's controller model class.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPaginated()
    {
        if (!empty($this->multipleAppends)) {
            $this->class::setStaticAppends($this->multipleAppends);
        }

        if (method_exists($this, 'beforeGetMultiple')) {
            $this->beforeGetMultiple();
        }

        $query = $this->class::query();
        $query->with($this->multipleRelationships);
        $orderBy = $this->orderBy ? $this->orderBy : $this->keyName;
        if ($this->orderAsc) {
            $query->orderBy($orderBy, 'ASC');
        } else {
            $query->orderBy($orderBy, 'DESC');
        }

        return response()->json($query->paginate($this->itemsPerPage));
    }

    /**
     * Checks if searchQuery method exists on parent controller otherwise
     * throws an error "No searchQuery method defined".
     *
     * @param bool $trashedOnly
     * @return mixed
     * @throws \Exception
     */
    public function getSearchQuery($trashedOnly = false)
    {
        if (method_exists($this, 'searchQuery')) {
            $results = $this->searchQuery($trashedOnly);
        } else {
            throw new \Exception('No searchQuery method defined');
        }

        if (method_exists($this, 'beforeSearch')) {
            $this->beforeSearch();
        }

        $orderBy = $this->orderBy ? $this->orderBy : $this->keyName;
        if ($this->orderAsc) {
            $results->orderBy($orderBy, 'ASC');
        } else {
            $results->orderBy($orderBy, 'DESC');
        }

        return $results;
    }

    /**
     * Returns search query for parent's controller model class.
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function search()
    {
        return response()->json([
            Str::plural($this->itemName) => $this->getSearchQuery()->get(),
        ]);
    }

    /**
     * Returns paginated search results for parent's controller model class.
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function searchPaginated()
    {
        if (!empty($this->multipleAppends)) {
            $this->class::setStaticAppends($this->multipleAppends);
        }

        return response()->json([
            Str::plural($this->itemName) => $this->getSearchQuery()->paginate($this->itemsPerPage),
        ]);
    }

    /**
     * Returns paginated search results for parent's deleted controller model class.
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function deletedPaginated()
    {
        if (!empty($this->multipleAppends)) {
            $this->class::setStaticAppends($this->multipleAppends);
        }

        return response()->json([
            Str::plural($this->itemName) => $this->getSearchQuery(true)->paginate($this->itemsPerPage),
        ]);
    }

    /**
     * Restores item from deleted state.
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function restore()
    {
        $i = $this->class::withTrashed()->with($this->singleRelationships)->find(request($this->keyName))->append($this->singleAppends);

        $i->restore();

        if (method_exists($this, 'afterRestore')) {
            $this->afterRestore($i);
        }

        return response()->json([
            $this->itemName => $i,
        ]);
    }

    /**
     * Custom paginator for collection.
     * @param $items
     * @param int $perPage
     * @param null $page
     * @param array $options
     * @return array
     */
    public function paginate($items, $perPage = 15, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        $pagination = new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page,
            $options);
        $pagination = $pagination->toArray();
        $pagination['data'] = array_values($pagination['data']);

        return $pagination;
    }


    /**
     * Specify which table columns to search for text and return result query.
     *
     * @param $query
     * @param $searchQuery
     * @return mixed
     */
    public function searchColumnsForText($query, $searchQuery)
    {
        $searchTerms = explode(' ', $searchQuery);
        $searchColumns = $this->searchColumns;

        $query->where(function ($q) use ($searchTerms, $searchColumns) {
            foreach ($searchTerms as $term) {
                $term = '%' . trim($term) . '%';

                $q->orWhere(function ($subQuery) use ($term, $searchColumns) {
                    $first = true;
                    foreach ($searchColumns as $searchColumn) {
                        if ($first) {
                            $subQuery->where($searchColumn, 'LIKE', $term);
                            $first = false;
                        } else {
                            $subQuery->orWhere($searchColumn, 'LIKE', $term);
                        }
                    }
                });
            }
        });

        return $query;
    }

    /**
     * Specify how to build search query with filter keys and return result query.
     *
     * @param $query
     * @param $filters
     * @return mixed
     */
    public function applyFilters($query, $filters)
    {
        foreach ($filters as $key => $ids) {
            if (empty($ids)) {
                continue;
            }
            switch ($key) {
                default:
                    break;
            }
        }

        return $query;
    }

    /**
     * If request has sort by key which is sent with vuetify table
     * sorting then we will order the requested column name by
     * descending or ascending.
     *
     * @param $query
     * @return mixed
     */
    public function checkSortBy($query)
    {
        if (request('sortBy', null)) {
            $sortDesc = request('sortDesc');

            $class = (new $this->class);
            $columns = $class->getConnection()->getSchemaBuilder()->getColumnListing($class->getTable());

            foreach (request('sortBy') as $key => $value) {
                if (!isset($sortDesc[$key])) $sortDesc[$key] = false;

                if (in_array($value, $columns)) {
                    $query->orderBy($value, $sortDesc[$key] ? 'asc' : 'desc');
                }
            }
        }

        return $query;
    }

    public function exportToExcel()
    {
        $query = $this->searchQuery()->with([]);
        $format = request('format', 'xlsx');
        return $this->exportQueryToExcel($query, $format);
    }

    public function exportQueryToExcel($query, $format = 'xlsx')
    {
        $records = [];

        foreach ($query->get() as $item) {
            $records[] = $this->exportToExcelRow($item);
        }

        if (!empty($records)) {
            $header = array_keys($records[0]);
        } else {
            $header = [];
        }

        if (in_array($format, ['csv', 'xls', 'xlsx'])) {
            return \Excel::download(new \App\ExcelExport(array_merge([$header], $records), $this->exportGetTitle()), $this->exportGetTitle() . '.' . $format);
        }
    }

    public function exportToExcelRow($item)
    {
        return [
            'id' => $item->id,
            'name' => $item->name,
        ];
    }

    // as is
    public function exportGetTitle()
    {
        return ucfirst($this->itemName . ' Records');
    }
}

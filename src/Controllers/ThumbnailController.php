<?php

namespace Wai\Adminify\Controllers;

use Wai\Adminify\Traits\HasThumbnail;

class ThumbnailController
{
    use HasThumbnail;

    public function generate($model, $modelParams)
    {
        return $this->resolveRequest($model, $modelParams);
    }
}

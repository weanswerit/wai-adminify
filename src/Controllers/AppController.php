<?php

namespace Wai\Adminify\Controllers;

class AppController
{
    public function any()
    {
        if (view()->exists('any')) {
            if (\Auth::user()) {
                return view('any');
            } else {
                return redirect()
                    ->route('login')
                    ->with('redirect', request()->getRequestUri());
            }
        } else {
            abort(404);
        }
    }

    public function token()
    {
        return response()->json(['token' => csrf_token()]);
    }

    public function favicon()
    {
        $manifest = [
            'name' => 'App',
            'icons' => []
        ];

        foreach (\App\Setting::getManifestFaviconUrls() as $faviconUrl) {
            $manifest['icons'][] = [
                'src' => $faviconUrl['src'],
                'sizes' => $faviconUrl['size'] . 'x' . $faviconUrl['size'],
                'type' => $faviconUrl['type'],
                'density' => $faviconUrl['density']
            ];
        }

        return response()->json($manifest);
    }

    public function browserconfig()
    {
        $config = '<?xml version="1.0" encoding="utf-8"?>';
        $config .= '<browserconfig><msapplication><tile>';

        foreach (\App\Setting::getBrowserConfigFaviconUrls() as $faviconUrl) {
            $config .= '<square' . $faviconUrl['size'] . 'x' . $faviconUrl['size'] . 'logo src="' . $faviconUrl['src'] . '"/>';
        }

        $config .= '<TileColor>#ffffff</TileColor></tile></msapplication></browserconfig>';

        return response($config, 200, ['Content-Type' => 'application/xml']);
    }
}

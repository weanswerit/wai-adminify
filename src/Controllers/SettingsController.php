<?php

namespace Wai\Adminify\Controllers;

use App\Setting;
use GeoSot\EnvEditor\EnvEditor;
use Wai\Adminify\Events\UpdateDom;
use GeoSot\EnvEditor\Exceptions\EnvException;

class SettingsController extends VueController
{
    protected $class = Setting::class;
    protected $itemName = 'setting';
    protected $keyName = 'param';

    protected $excludeFromDB = [
        'application', 'google', 'mail'
    ];

    public function getAll()
    {
        $dbSettings = Setting::getAll();
        $envSettings = Setting::getEnvSettings();
        $configSettings = Setting::getConfigSettings();
        $images = Setting::getMedia();

        $settings = array_merge($dbSettings, $envSettings, $configSettings, $images);

        return response()->json(['settings' => $settings]);
    }

    public function updateAll()
    {
        $settings = request('settings');

        self::createBackupForApplication();

        foreach ($settings as $param => $value) {
            if ($param == 'images') {
                continue;
            }

            if (in_array($param, $this->excludeFromDB)) {
                self::updateEnv($param, $value);
            } else {
                Setting::set($param, $value);
            }
        }

        self::setCacheForApplication();

        event(new UpdateDom('settings'));
    }

    public function getAppIcon()
    {
        $mediaSettings = (array)Setting::get(Setting::MEDIA);

        if ($mediaSettings['useFaviconAsAppIcon']) {
            $favicon = Setting::getFavicon();

            if (is_null($favicon)) {
                $favicon['icon'] = $mediaSettings['appIcon'];
                $favicon['color'] = $mediaSettings['appIconColor'];
            }
        } else {
            $favicon['icon'] = $mediaSettings['appIcon'];
            $favicon['color'] = $mediaSettings['appIconColor'];
        }

        return response()->json(['icon' => $favicon]);
    }

    public function clearCache()
    {
        \Artisan::call('cache:clear');
    }

    private function createBackupForApplication()
    {
        $env = new EnvEditor();
        $env->backUpCurrent();
    }

    private function updateEnv($param, $data)
    {
        switch ($param) {
            case 'application':
                $data = [
                    'APP_NAME' => '"' . $data['name'] . '"',
                    'APP_DEBUG' => $data['debug'] ? 'true' : 'false',
                    'DEBUGBAR_ENABLED' => $data['debugbar'] ? 'true' : 'false',
                    'APP_ENV' => $data['production'] ? 'production' : 'local',
                    'APP_MAIL_DEBUG' => $data['mail_debug'] ? 'true' : 'false',
                    'QUEUE_ENABLED' => $data['jobs_enabled'] ? 'true' : 'false',
                    'APP_REGISTER' => $data['register'] ? 'true' : 'false',
                    'APP_SOCIAL_LOGIN' => $data['social_login'] ? 'true' : 'false',
                    'APP_EMAIL_VERIFICATION' => $data['email_verification'] ? 'true' : 'false',
                ];
                $this->setEnv($data);
                break;
            case 'google':
                $data = [
                    'GOOGLE_APP_ID' => $data['id'],
                    'GOOGLE_APP_SECRET' => $data['secret'],
                    'GOOGLE_REDIRECT_URL' => $data['redirect_url'],
                ];
                $this->setEnv($data);
                break;
            case 'mail':
                $data = [
                    'APP_EMAIL' => $data['email'],
                    'APP_SUPPORT_EMAIL' => $data['support_email'],
                    'MAIL_FROM_NAME' => '"' . $data['from_name'] . '"',
                    'MAIL_FROM_ADDRESS' => $data['from_email'],
                ];
                $this->setEnv($data);
                break;
            default:
                break;
        }
    }

    private function setEnv($data)
    {
        $env = new EnvEditor();

        foreach ($data as $key => $value) {
            try {
                $env->editKey($key, $value);
            } catch (EnvException $e) {
                $env->addKey($key, $value);
            }
        }
    }

    private function setCacheForApplication()
    {
        \Artisan::call('cache:clear');

        if (config('app.env', 'local') == 'local') {
            \Artisan::call('config:clear');
        } else {
            \Artisan::call('config:cache');
        }
    }

}

<?php

namespace Wai\Adminify\Models;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{

    protected $appends = ['slug'];

    /**
     * A user can have multiple roles, so each role can have many users.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(User::class, 'user_role');
    }

    /**
     * Returns sluggable name.
     *
     * @return string
     */
    public function getSlugAttribute()
    {
        return Str::slug($this->name);
    }

    public function getUserIdsAttribute()
    {
        return $this->users()->pluck('id');
    }

    public function setUserIdsAttribute()
    {
        return;
    }
}

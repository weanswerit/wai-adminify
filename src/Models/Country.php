<?php

namespace Wai\Adminify\Models;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $table = 'countries';
    protected $primaryKey = 'iso';
    public $timestamps = false;

    protected $casts = [
        'iso' => 'string'
    ];

    public static $countries = false;

    /**
     * Returns the calling prefix based on the ISO-2 code of the country
     *
     * @param string $iso Two letters ISO code
     * @return string Calling prefix of the country
     * @author Mircea Nicolae <mircea@nicolae.co.za>
     *
     */
    public static function getPrefixByIso($iso)
    {
        try {
            return Country::query()->find(strtoupper($iso))->prefix;
        } catch (Exception $e) {

        }
        return;
    }


    /**
     * Returns the country name by ISO-2 code
     *
     * @param string $iso Two letters ISO code
     * @return string Calling prefix of the country
     * @author Mircea Nicolae <mircea@nicolae.co.za>
     *
     */
    public static function getCountryByIso($iso)
    {
        try {
            return Country::query()->find(strtoupper($iso))->name;
        } catch (\ErrorException $e) {

        }
        return;
    }

    /**
     * Returns an associative array (ISO=>NAME) of all the countries
     *
     * @return array
     * @author Mircea Nicolae <mircea@nicolae.co.za>
     *
     */
    public static function getIsoCountries()
    {
        return Country::query()->orderBy('name')->pluck('name', 'iso')->toArray();
    }

    /**
     * Returns the ISO-2 code based on a telephone number
     *
     * @param string $number Telephone number
     * @return string ISO-2 country code
     * @author Mircea Nicolae <mircea@nicolae.co.za>
     *
     */
    public static function findIsoByNumber($number)
    {
        if (!static::$countries) {
            static::$countries = Country::query()->select('iso', \DB::raw('CONCAT("00", prefix) as `prefix`'))
                ->where('prefix', '!=', '')
                // excluding less important countries that have the same prefix
                ->whereNotIn('iso', ['PR', 'CA', 'EH', 'YT', 'TF', 'AX', 'JE', 'GG', 'IM', 'SJ',
                    'BV', 'GS', 'MF', 'BL', 'BQ', 'AN', 'CX', 'CC', 'HM', 'AQ', 'KZ'])
                ->orderBy(\DB::raw('CHAR_LENGTH(`prefix`)'), 'DESC')
                ->orderBy('iso', 'DESC')
                ->pluck('prefix', 'iso')
                ->toArray();
        }

        $country = '';

        foreach (static::$countries as $iso => $prefix) {
            if (Str::startsWith($number, $prefix)) {
                $country = $iso;
                break;
            }
        }

        if ($country == 'US') { //checking for oh, Canada
            $number = ltrim($number, '0');
            $localPrefix = substr($number, 1, 3);
            if (in_array($localPrefix, ['403', '587', '780', '236', '250', '604', '778', '204', '431',
                '506', '709', '902', '782', '226', '249', '289', '343', '365', '416', '437', '519',
                '613', '647', '705', '807', '905', '418', '438', '450', '514', '579', '581', '819',
                '873', '306', '639', '867'])) {
                $country = 'CA';
            }
        }

        return $country;
    }

    /**
     * Return Prefix instance based on a telephone number
     *
     * @param string $number Telephone number
     * @return Country|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|Model
     * @author Mircea Nicolae <mircea@nicolae.co.za>
     *
     */
    public static function findCountryByNumber($number)
    {
        return Country::query()->find(Country::findIsoByNumber($number));
    }

    /**
     * @param $number
     * @param int $country
     * @return mixed|string
     */
    function cleanNumber($number, $country = 0)
    {
        if ($country === 0 or $country == 'ZA' or $country == 'EU') {
            $number = str_replace('+27', '0', $number);
            $number = str_replace('(0)', '', $number);
            $number = str_replace('+', '00', $number);
            $number = trim(preg_replace('/[^0-9]/', '', $number));
            if (!empty($number) and substr($number, 0, 1) != '0' and strlen($number) > 5) {
                $number = '00' . $number;
            }
        } else {
            $country = strtoupper($country);

            $number = str_replace('+', '00', $number);
            $number = trim(preg_replace('/[^0-9]/', '', $number));

            if (substr($number, 0, 2) != '00') {
                $number = ltrim($number, '0');
                $prefix = Country::getPrefixByIso($country);
                if (Str::startsWith($number, $prefix)) {
                    $number = substr($number, strlen($prefix));
                }

                $number = '00' . $prefix . ltrim($number, '0');
            } else {
                $prefix = Country::getPrefixByIso($country);
                if (Str::startsWith(ltrim($number, '00'), $prefix)) {
                    $number = substr($number, 2 + strlen($prefix));
                    $number = '00' . $prefix . ltrim($number, '0');
                }
            }
        }

        return $number;
    }

    /**
     * @param $uglyCallerId
     * @return string
     */
    function cleanCallerId($uglyCallerId)
    {
        setlocale(LC_ALL, config('app.locale'));
        $callerId = iconv("UTF-8", "ASCII//TRANSLIT", $uglyCallerId);
        $callerId = trim(preg_replace('/\s+/', ' ', $callerId));
        return $callerId;
    }
}

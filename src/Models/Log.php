<?php

namespace Wai\Adminify\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Log extends Model
{

    protected $dates = ['created_at'];

    /**
     * A log belongs to a user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * A log can belong to an associated model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function association()
    {
        return $this->morphTo();
    }

    /**
     * Converting details column from json to array.
     *
     * @return mixed
     */
    public function getDetailsAttribute()
    {
        $details = json_decode($this->attributes['details'], true);

        if (!isset($details['impersonator'])) $details['impersonator'] = null;

        return $details;
    }

    /**
     * Converting details from array to json to store in database.
     *
     * @param $value
     */
    public function setDetailsAttribute($value)
    {
        $details = !isset($this->attributes['details']) ? [] : json_decode($this->attributes['details'], true);

        foreach ($value as $p => $v) {
            $details[$p] = $v;
        }
        $this->attributes['details'] = json_encode($details);
    }

    /**
     * Creates a log entry.
     *
     * @param $logType
     * @param null $association
     * @param null $user
     * @param array $details
     * @return Log
     */
    public static function create($logType, $association = null, $user = null, $details = [])
    {
        $l = new static();

        if (!is_null($user)) {
            $l->user()->associate($user);
        } else {
            $l->user()->associate(\Auth::user());
        }

        if (!is_null($association)) {
            $l->association()->associate($association);
        }

        $l->type = $logType;

        $impersonator = \Session::pull('original_user');

        if (is_null($impersonator)) {
            $l->details = $details;
        } else {
            $l->details = array_merge($details, ['impersonator_id' => $impersonator]);
        }

        $l->save();

        return $l;
    }

    /**
     * Checks whether a log type is in defined constants of log class.
     *
     * @param $logType
     * @return bool
     * @throws \ReflectionException
     */
    public static function checkIfValidLogType($logType)
    {
        $class = new \ReflectionClass(static::class);
        return in_array($logType, $class->getConstants());
    }

    /**
     * Tries to retrieve log constant based on model and action
     * eg. $class = USER_CREATED should return Log::LOG_USER_CREATED.
     *
     * @param $className
     * @return bool|mixed
     * @throws \ReflectionException
     */
    public static function getLogTypeFromClass($className)
    {
        $constant = 'LOG_' . strtoupper($className);

        $class = new \ReflectionClass(static::class);
        if (array_key_exists($constant, $class->getConstants())) {
            return $class->getConstants()[$constant];
        } else {
            return false;
        }
    }

    /**
     * Tries to retrieve log constant based on model and action
     * eg. $class = USER_CREATED should return Log::LOG_USER_CREATED.
     *
     * @param $type
     * @return bool|mixed
     * @throws \ReflectionException
     */
    public static function getLogClassFromType($type)
    {
        $class = new \ReflectionClass(static::class);

        $constants = $class->getConstants();

        if (in_array($type, $constants)) {
            return array_search($type, $constants);
        } else {
            return '';
        }
    }

    /**
     * @param $type
     * @return string
     * @throws \ReflectionException
     */
    public static function getLabel($type)
    {
        $label = static::getLogClassFromType($type);
        $label = str_replace('LOG', '', $label);
        $label = str_replace('_', ' ', $label);
        return ucfirst(strtolower(trim($label)));
    }

    /**
     * @return string
     * @throws \ReflectionException
     */
    public function getActionLabelAttribute()
    {
        return static::getLabel($this->type);
    }

    /**
     *
     */
    public function setActionLabelAttribute()
    {
        return;
    }

    /**
     * @return string
     */
    public function getUserNameAttribute()
    {
        return $this->user->full_name;
    }

    /**
     *
     */
    public function setUserNameAttribute()
    {
        return;
    }

    public function getDescriptionAttribute()
    {
        $description = '';
        $description .= $this->user->full_name;

        if (isset($this->details['impersonator']) and !is_null($this->details['impersonator'])) {
            $description .= ', impersonated by ' . User::query()->find($this->details['impersonator'])->full_name . ', ';
        } else {
            $description .= ' ';
        }

        $action = static::getLabel($this->type);
        $actionArray = explode(' ', $action);
        $reversedArray = array_reverse($actionArray);

        $description .= $reversedArray[0] . ' a ' . strtolower(implode(' ', array_slice($reversedArray, 1)));

        $description .= ' at ' . $this->created_at . ' (' . $this->created_at->fromNow() . ')';

        return $description;
    }

    public function setDescriptionAttribute()
    {
        return;
    }

    public static function exportCollectionForExcel($collection, $format = 'xlsx')
    {
        $records = [];

        foreach ($collection as $row) {
            $records[] = [
                'id' => $row->id,
                'user_id' => $row->user_id,
                'timestamp' => $row->created_at,
                'description' => $row->description,
                'object_type' => $row->association_type,
                'object_id' => $row->association_id,
            ];
        }

        $header = array_keys($row->toArray());

        if (in_array($format, ['csv', 'xls', 'xlsx'])) {
            return \Excel::download(new \App\ExcelExport(array_merge([$header], $records), 'Log Records'), 'Log Records.' . $format);
        }
    }

}

<?php

namespace Wai\Adminify\Models;

class ExcelExport implements \Maatwebsite\Excel\Concerns\FromCollection,
    \Maatwebsite\Excel\Concerns\WithHeadings,
    \Maatwebsite\Excel\Concerns\ShouldAutoSize,
    \Maatwebsite\Excel\Concerns\WithTitle
{
    public $title = 'Worksheet';
    public $records = [];

    function __construct($records, $title = 'Worksheet')
    {
        $this->title = $title;
        $this->records = $records;
    }

    public function title(): string
    {
        return $this->title;
    }

    public function headings(): array
    {
        return array_slice($this->records, 0, 1);
    }

    public function collection()
    {
        return collect(array_slice($this->records, 1));
    }
}

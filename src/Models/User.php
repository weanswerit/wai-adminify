<?php

namespace Wai\Adminify\Models;

use ColorTools\HasImages;
use Wai\Adminify\Traits\Loggable;
use Wai\Adminify\Traits\Notifiable;
use Illuminate\Support\Facades\Hash;
use Wai\Adminify\Traits\HasThumbnail;
use Wai\Adminify\Jobs\MailUserVerifyEmail;
use Wai\Adminify\Jobs\MailUserPasswordReset;
use Illuminate\Database\Eloquent\SoftDeletes;
use Wai\Adminify\Traits\DynamicChangeAppends;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements MustVerifyEmail
{
    use Loggable;
    use HasImages;
    use Notifiable;
    use SoftDeletes;
    use HasThumbnail;
    use DynamicChangeAppends;

    protected $table = 'users';

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->mergeAttributes();
    }

    private function mergeAttributes()
    {
        $this->appends = array_unique(array_merge($this->appends, [
            'full_name'
        ]));
        $this->dates = array_unique(array_merge($this->dates, [
            'deleted_at'
        ]));
        $this->fillable = array_unique(array_merge($this->fillable, [
            'first_name', 'last_name', 'email', 'password', 'details'
        ]));
        $this->hidden = array_unique(array_merge($this->hidden, [
            'password', 'remember_token'
        ]));
        $this->casts = array_replace($this->casts, [
            'email_verified_at' => 'datetime',
            'last_login' => 'datetime',
        ]);
    }

    public function sendPasswordResetNotification($token)
    {
        dispatch_now(new MailUserPasswordReset($this->email, $token));
    }

    public function sendEmailVerificationNotification()
    {
        dispatch_now(new MailUserVerifyEmail($this->email));
    }

    /**
     * Converting details column from json to array.
     *
     * @return mixed
     */
    public function getDetailsAttribute()
    {
        return json_decode($this->attributes['details'], true);
    }

    /**
     * Finds admin by email.
     *
     * @param $email
     * @param array $appends
     * @return \Illuminate\Database\Eloquent\Model|null|object|static
     */
    public static function findByEmail($email, $appends = [])
    {
        return static::query()->where('email', $email)->with($appends)->first();
    }

    /**
     * Converting details from array to json to store in database.
     *
     * @param $value
     */
    public function setDetailsAttribute($value)
    {
        $details = !isset($this->attributes['details']) ? [] : json_decode($this->attributes['details'], true);

        foreach ($value as $p => $v) {
            $details[$p] = $v;
        }
        $this->attributes['details'] = json_encode($details);
    }

    /**
     * Stores the password securely in database.
     *
     * @param $password
     */
    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = Hash::make($password);
    }

    /**
     * A admin can have many roles.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany(\App\Role::class, 'user_role');
    }

    /**
     * Getting only the role ids for a user.
     *
     * @return \Illuminate\Support\Collection
     */
    public function getRoleIdsAttribute()
    {
        return $this->roles()->pluck('id');
    }

    /**
     * Setting the role ids for a user.
     *
     * @param $roleIds
     * @return array
     */
    public function setRoleIdsAttribute($roleIds)
    {
        return $this->roles()->sync($roleIds);
    }

    /**
     * Returns user's first name and last name.
     *
     * @return string
     */
    public function getFullNameAttribute()
    {
        return trim($this->first_name . ' ' . $this->last_name);
    }

    /**
     * Sets user's name and surname.
     *
     */
    public function setFullNameAttribute()
    {
        return;
    }

    /**
     * Returns user's mobile number.
     *
     * @return string
     */
    public function getMobileAttribute()
    {
        return trim('+' . $this->details['number_prefix'] . ' ' . $this->details['number']);
    }

    /**
     * Sets number.
     *
     */
    public function setMobileAttribute()
    {
        return;
    }

    /**
     * @param $role
     * @return bool
     */
    public function hasRole($role)
    {
        return in_array($role, $this->role_ids->toArray());
    }

    /**
     * Marks a user as updated
     * @return \App\User
     */
    public function touch()
    {
        $this->updated_at = date('Y-m-d H:i:s');
        $this->save();
        return $this;
    }

    /**
     * Gets the delegated users
     * @param bool $includeSelf
     * @return mixed
     */
    public function getDelegatedUser(bool $includeSelf = true)
    {
        return static::whereIn('id', $this->getDelegatedUser($includeSelf));
    }

    /**
     * Gets the delegated users' ids
     * @param bool $includeSelf
     * @return array
     */
    public function getDelegatedUserIds(bool $includeSelf = true)
    {
        if ($this->hasRole(\App\Role::ADMIN) or $this->hasRole(\App\Role::SUPER_ADMIN)) {
            $delegatedUserIds = static::pluck('id')->toArray();
        }

        if ($includeSelf) {
            $delegatedUserIds[] = $this->id;
        }

        return array_unique($delegatedUserIds);
    }

    /**
     * Returns the latest updated timestamp
     * @return int
     */
    public static function getLatestTimestamp()
    {
        return strtotime(static::max('updated_at'));
    }

    /**
     * Gets the use resources
     * @param int $lastTimestamp
     * @return mixed
     */
    public function getUserResources(int $lastTimestamp = 0)
    {
        $resources['timestamp'] = static::getLatestTimestamp();

        if ($resources['timestamp'] <= $lastTimestamp) {
            return $resources;
        }

        $columns = [
            'id',
            'first_name',
            'last_name',
            'email',
            'details',
            'updated_at',
        ];

        static::setStaticAppends([
            'thumbnail',
            'full_name',
            'mobile',
            'role_ids',
        ]);
        $resources['users'] = static::get($columns);
        $resources['delegatedIds'] = $this->getDelegatedUserIds();

        return $resources;
    }

    /**
     * Marks a user as updated by id - returns true if successful
     * @param int $userId
     * @return bool
     */
    public static function touchById(int $userId)
    {
        $user = static::query()->find($userId);
        if ($user) {
            $user->touch();
            return true;
        }
    }

    /**
     * @param int $role
     * @return mixed
     */
    public static function byRole(int $role)
    {
        return static::query()->whereHas('roles', function ($r) use ($role) {
            $r->where('id', $role);
        });
    }

    /**
     * @param int $role
     * @return mixed
     */
    public static function getByRole(int $role)
    {
        return static::byRole($role)->get();
    }

    /**
     * @param array $roles
     * @return mixed
     */
    public static function byRoles(array $roles = [])
    {
        return static::query()->whereHas('roles', function ($r) use ($roles) {
            $r->whereIn('id', $roles);
        });
    }

    /**
     * @param array $roles
     * @return mixed
     */
    public static function getByRoles(array $roles = [])
    {
        return static::byRoles($roles)->get();
    }

    /**
     * Returns the next $limit birthdays - if limit is 0 (default) it will return all the birthdays
     * @param int $limit
     * @return array
     */
    public static function getUpcomingBirthdays($limit = 0)
    {
        $today = date('Y-m-d');
        $thisYear = [];
        $nextYear = [];
        foreach (static::all() as $user) {
            if (isset($user->details['birth_date']) and !empty($user->details['birth_date'])) {
                $birthday = substr($today, 0, 4) . '-' . substr($user->details['birth_date'], 5);
                if ($birthday < $today) {
                    $nextYear[$user->id] = (1 + substr($today, 0, 4)) . '-' . substr($user->details['birth_date'], 5);
                } else {
                    $thisYear[$user->id] = substr($today, 0, 4) . '-' . substr($user->details['birth_date'], 5);
                }
            }
        }

        $birthdays = array_sort($thisYear) + array_sort($nextYear);
        if ($limit) {
            $birthdays = array_slice($birthdays, 0, $limit, true);
        }

        return $birthdays;
    }

    /**
     * Gets the impersonator's user
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
     */
    public function getImpersonatorAttribute()
    {
        $id = session()->get('impersonator_id');
        if (!is_null($id)) {
            return static::query()->find($id);
        } else {
            return null;
        }
    }

    /**
     * Stub
     */
    public function setImpersonatorAttribute()
    {
        return;
    }

    /**
     * Starts impersonating user by id
     * @param $userId
     */
    public static function impersonateLogin($userId)
    {
        $user = \Auth::user();
        if ($user and $user->hasRole(\App\Role::SUPER_ADMIN)) {
            \Session::put('impersonator_id', $user->id);
            \Auth::loginUsingId($userId);
        }
    }

    /**
     * Returns from the impersonated state
     */
    public static function impersonateReturn()
    {
        $id = \Session::pull('impersonator_id');
        if ($id) {
            \Auth::loginUsingId($id);
        }
    }
}

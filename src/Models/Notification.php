<?php

namespace Wai\Adminify\Models;

use App\User;
use Illuminate\Support\Str;
use Laravolt\Avatar\Avatar;
use Illuminate\Database\Eloquent\Model;
use Wai\Adminify\Events\UpdateNotifications;
use Illuminate\Notifications\DatabaseNotification;
use Symfony\Component\Debug\Exception\FatalThrowableError;

class Notification extends DatabaseNotification
{
    const ACTION_METHOD_URL_OPEN = 0;
    const ACTION_METHOD_URL_ROUTE = 1;

    protected $debug = false;
    protected $notificationClass = 'Wai\\Adminify\\Notifications\\BaseNotification';

    protected $initialData = [
        'actionMethod' => self::ACTION_METHOD_URL_ROUTE,
        'consume' => false,
        'groupConsume' => false,
        'excludeActor' => true,
    ];

    protected $recipients = [];

    public function setDebug(bool $debug)
    {
        $this->debug = $debug;
        return $this;
    }

    public function enableDebug()
    {
        return $this->setDebug(true);
    }

    public function disableDebug()
    {
        return $this->setDebug(false);
    }


    public function setExcludeActor(bool $excludeActor)
    {
        $this->initialData['excludeActor'] = $excludeActor;
        return $this;
    }

    public function enableExcludeActor()
    {
        return $this->setExcludeActor(true);
    }

    public function disableExcludeActor()
    {
        return $this->setExcludeActor(false);
    }

    public function allowActorRecipient()
    {
        return $this->disableExcludeActor();
    }


    public function setConsume(bool $consume)
    {
        $this->initialData['consume'] = $consume;
        return $this;
    }

    public function enableConsume()
    {
        return $this->setConsume(true);
    }

    public function disableConsume()
    {
        return $this->setConsume(false);
    }


    public function setGroupConsume(bool $groupConsume)
    {
        $this->initialData['groupConsume'] = $groupConsume;
        return $this;
    }

    public function enableGroupConsume()
    {
        return $this->setGroupConsume(true);
    }

    public function disableGroupConsume()
    {
        return $this->setGroupConsume(false);
    }


    public function setActor($actor)
    {
        if (is_numeric($actor)) {
            $user = \App\User::query()->where('id', $actor)->first();

            if (!$user && $this->debug) {
                throw new \Exception('Could not find user by id:' . $actor);
            }
        } elseif ($actor instanceof \App\User) {
            $actor = $actor->id;
        } else {
            if ($this->debug) {
                throw new \Exception('I don\'t know who the owner is');
            }
        }

        $this->initialData['actor'] = $actor;
        return $this;
    }


    public function setObject(Model $object)
    {
        if ($object instanceof Model) {
            $this->initialData['object'] = [
                'id' => $object->{$object->primaryKey},
                'type' => get_class($object),
            ];
        } else {
            if ($this->debug) {
                throw new \Exception('Item must be instance of Illuminate\Database\Eloquent\Model');
            }
        }

        return $this;
    }

    public function setAction(string $action)
    {
        $this->initialData['action'] = $action;

        return $this;
    }

    public function setMessage(string $message)
    {
        $this->initialData['message'] = $message;
        return $this;
    }

    public function setVerb(string $verb)
    {
        $this->initialData['verb'] = $verb;
        return $this;
    }

    public function setActionUrl(string $actionUrl)
    {
        $this->initialData['actionUrl'] = $actionUrl;
        return $this;
    }

    public function setActionMethod(int $actionMethod)
    {
        if (!in_array($actionMethod, [
                static::ACTION_METHOD_URL_OPEN,
                static::ACTION_METHOD_URL_ROUTE,
            ]) and $this->debug) {
            throw new \Exception('Invalid action method: ' . $actionMethod);
        }

        $this->initialData['actionMethod'] = $actionMethod;
        return $this;
    }

    private function addRecipientsIds($ids)
    {
        $this->recipients = array_values(array_unique(array_merge($this->recipients, $ids)));
    }

    private function removeRecipientsIds($ids)
    {
        $this->recipients = array_values(array_unique(array_diff($this->recipients, $ids)));
    }

    public function addRecipientsByRoles($roles)
    {
        $roles = collect($roles);
        foreach ($roles as $role) {
            if ($role instanceof \App\Role) {
                $this->addRecipientsIds($role->users()->pluck('id')->toArray());
            } else if (is_int($role)) {
                $this->addRecipientsIds(\App\User::byRole($role)->pluck('id')->toArray());
            }
        }

        return $this;
    }

    public function addRecipients($recipients)
    {
        $recipients = collect($recipients);
        foreach ($recipients as $recipient) {
            if ($recipient instanceof \App\User) {
                $this->addRecipientsIds([$recipient->id]);
            } else if (is_int($recipient)) {
                $this->addRecipientsIds([$recipient]);
            }
        }

        return $this;
    }

    public function removeRecipients($recipients)
    {
        $recipients = collect($recipients);
        foreach ($recipients as $recipient) {
            if ($recipient instanceof \App\User) {
                $this->removeRecipientsIds([$recipient->id]);
            } else if (is_int($recipient)) {
                $this->removeRecipientsIds([$recipient]);
            }
        }

        return $this;
    }

    private function excludeActor()
    {
        if (isset($this->initialData['actor'])) {
            $this->removeRecipientsIds([$this->initialData['actor']]);
        }
        return $this;
    }

    private function preDispatchChecks()
    {
        $validatorData = \Validator::make($this->initialData, [
            'action' => 'required|string',
            'message' => 'string',
            'actionUrl' => 'string',
            'actionMethod' => 'required|numeric',
            'actor' => 'required|numeric',
            'consume' => 'required|boolean',
            'groupConsume' => 'required|boolean',
            'excludeActor' => 'required|boolean',
        ]);

        if ($validatorData->fails()) {
            throw new \Exception($validator->errors());
        }

        return $this;
    }

    public function dispatch()
    {
        $this->preDispatchChecks();

        if ($this->initialData['excludeActor']) {
            $this->excludeActor();
        }

        $notificationClass = $this->notificationClass;
        foreach ($this->recipients as $recipient) {
            try {
                $user = \App\User::find($recipient);
                $user->notify(new $notificationClass($this->initialData));
                event(new UpdateNotifications($user->id));
            } catch (\Throwable $e) {
                if ($this->debug) {
                    throw new \Exception('Notification Error: ' . $e->getMessage());
                }
                break;
            }
        }

        return true;
    }

    public function getActionUrl()
    {
        if (isset($this->data['actionUrl']) and !empty($this->data['actionUrl'])) {
            return $this->data['actionUrl'];
        }
    }

    public function getMessage()
    {
        if (isset($this->data['message']) and !empty($this->data['message'])) {
            return $this->data['message'];
        }

        $actor = $this->getActor();
        $object = $this->getObject();

        if (isset($this->data['verb']) and !empty($this->data['verb'])) {
            if (!is_null($object->full_name)) {
                return $actor->full_name . ' ' . $this->data['verb'] . ' ' . $object->full_name;
            } else if (!is_null($object->name)) {
                return $actor->full_name . ' ' . $this->data['verb'] . ' ' . $object->name;
            } else if (!is_null($object->id)) {
                return $actor->full_name . ' ' . $this->data['verb'] . ' ' . $object->id;
            } else {
                return $actor->full_name . ' ' . $this->data['verb'];
            }
        }

        if (!is_null($object->full_name)) {
            return $actor->full_name . ' ' . $this->data['action'] . ' ' . $object->full_name;
        } else if (!is_null($object->name)) {
            return $actor->full_name . ' ' . $this->data['action'] . ' ' . $object->name;
        } else if (!is_null($object->id)) {
            return $actor->full_name . ' ' . $this->data['action'] . ' ' . $object->id;
        } else {
            return $actor->full_name . ' ' . $this->data['action'];
        }
    }

    public function getActor()
    {
        return \App\User::withTrashed()->find($this->data['actor']);
    }

    public function getObject()
    {
        $class = $this->data['object']['type'];

        $traits = (new \ReflectionClass($class))->getTraits();

        if (array_key_exists('Illuminate\Database\Eloquent\SoftDeletes', $traits)) {
            $object = $class::withTrashed()->find($this->data['object']['id']);
        } else {
            $object = $class::query()->find($this->data['object']['id']);
        }

        return $object;
    }

    public function getJson()
    {
        return [
            'id' => $this->id,
            'thumbnail' => $this->thumbnail,
            'message' => $this->getMessage(),
            //'actor' => $this->getActor(), // ???
            //'object' => $this->getObject(), // ???
            'actionUrl' => $this->getActionUrl(),
            'actionMethod' => $this->data['actionMethod'],
            'created_at' => $this->created_at->format('Y-m-d H:i:s'),
        ];
    }

    public function getThumbnailAttribute()
    {
        $thumbnail = '';

        if (isset($this->data['actor']) and !empty($this->data['actor'])) {
            $actor = \App\User::withTrashed()->find($this->data['actor']);
            return $actor->getThumbnailUrl();
        }

        return $thumbnail;
    }

    public function setThumbnailAttribute()
    {
        return;
    }

    public static function notifyRoles(string $action, Model $object, $roles)
    {
        $notification = new self();
        $notification->setActor(\Auth::user());
        $notification->setObject($object);
        $notification->setAction($action);
        $notification->addRecipientsByRoles($roles);
        return $notification;
    }

    public static function notifyUsers(string $action, Model $object, $users)
    {
        $notification = new self();
        $notification->setActor(\Auth::user());
        $notification->setObject($object);
        $notification->setAction($action);
        $notification->addRecipients($users);
        return $notification;
    }
}

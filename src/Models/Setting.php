<?php

namespace Wai\Adminify\Models;

use FileTools\HasFiles;
use ColorTools\HasImages;
use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    use HasImages;
    use HasFiles;

    const APPLICATION = 1;
    const MEDIA = 2;
    const CREDENTIALS = 3;
    const CONTACT = 4;
    const EMAILS = 5;

    public static $paramsIds = [
        self::APPLICATION => 'application',
        self::MEDIA => 'media',
        self::CREDENTIALS => 'credentials',
        self::CONTACT => 'contact',
        self::EMAILS => 'emails',
    ];

    public static function getParamId($param)
    {
        $id = array_search($param, static::$paramsIds);
        if ($id) {
            return $id;
        }
    }

    public static function getParamFromId($id)
    {
        if (isset(static::$paramsIds[$id])) {
            return static::$paramsIds[$id];
        }
    }

    protected $table = 'settings';
    protected $fillable = ['id', 'param', 'value'];
    protected $casts = [
        'param' => 'string',
        'value' => 'object'
    ];

    public function getValueAttribute()
    {
        return (object)json_decode($this->attributes['value']);
    }

    public function setValueAttribute($value)
    {
        $this->attributes['value'] = json_encode($value);
    }

    public static function find($param)
    {
        if (!is_numeric($param)) {
            $param = static::getParamId($param);
        }
        return static::where('id', $param)->first();
    }

    public static function get($param, $defaultValue = null, $doNotAcceptEmpty = false)
    {
        $setting = static::find($param);

        if (is_null($setting)) {
            return $defaultValue;
        } else {
            if ($doNotAcceptEmpty and empty($setting->value)) {
                return $defaultValue;
            } else {
                return $setting->value;
            }
        }
    }

    public static function set($param, $value = '')
    {
        if (!is_numeric($param)) {
            $id = static::getParamId($param);
        }

        if (empty($id)) {
            throw new \Exception('Unknown parameter: ' . $param);
        }

        $setting = static::query()->firstOrNew(array('id' => $id, 'param' => $param));
        $setting->value = $value;
        $setting->save();

        return $value;
    }

    public static function getAll()
    {
        return static::query()->pluck('value', 'param')->toArray();
    }

    public static function getFavicon()
    {
        return static::find(static::MEDIA)->imageByRole('favicon');
    }

    public static function getLogo()
    {
        return static::find(static::MEDIA)->imageByRole('logo');
    }

    public static function getManifestFaviconUrls()
    {
        $dimensions = [
            '0.75' => '36',
            '1.0' => '48',
            '1.5' => '72',
            '2.0' => '96',
            '3.0' => '144',
            '4.0' => '192'
        ];

        return static::processDimensions($dimensions);
    }

    public static function getBrowserConfigFaviconUrls()
    {
        $dimensions = ['70', '150', '310'];

        return static::processDimensions($dimensions);
    }

    public static function getFaviconUrls()
    {
        return [
            'apple' => static::getAppleFaviconUrls(),
            'android' => static::getAndroidFaviconUrls(),
            'microsoft' => static::getMicrosoftFaviconUrls()
        ];
    }

    public static function getAppleFaviconUrls()
    {
        $dimensions = ['57', '60', '72', '76', '114', '120', '144', '152', '180'];

        return static::processDimensions($dimensions);
    }

    public static function getAndroidFaviconUrls()
    {
        $dimensions = ['16', '32', '96', '192'];

        return static::processDimensions($dimensions);
    }

    public static function getMicrosoftFaviconUrls()
    {
        $dimensions = ['144'];

        return static::processDimensions($dimensions);
    }

    private static function processDimensions($dimensions)
    {
        $favicon = static::getFavicon();

        if (!$favicon) {
            return [];
        }

        $urls = [];

        foreach ($dimensions as $density => $size) {
            $urls[] = [
                'size' => $size,
                'density' => $density,
                'type' => $favicon->metadata->mime,
                'src' => $favicon->getUrl(function (\ColorTools\Image $image) use ($size) {
                    $image->resizeContain($size, $size);
                }, $favicon->type == 'png' ? 'png' : 'jpeg')
            ];
        }

        return $urls;
    }

    public static function getEnvSettings()
    {
        return [
            'application' => [
                'name' => env('APP_NAME', 'WeAnswerIt'),
                'debug' => env('APP_DEBUG', true),
                'debugbar' => env('DEBUGBAR_ENABLED', false),
                'production' => env('APP_ENV', 'local') == 'local' ? false : true,
                'mail_debug' => env('APP_MAIL_DEBUG', false),
                'jobs_enabled' => env('QUEUE_ENABLED', true),
                'register' => env('APP_REGISTER', false),
                'social_login' => env('APP_SOCIAL_LOGIN', false),
                'email_verification' => env('APP_EMAIL_VERIFICATION', true),
            ],
            'google' => [
                'id' => env('GOOGLE_APP_ID', ''),
                'secret' => env('GOOGLE_APP_SECRET', ''),
                'redirect_url' => env('GOOGLE_REDIRECT_URL', ''),
            ]
        ];
    }

    public static function getConfigSettings()
    {
        return [
            'mail' => [
                'from_name' => env('MAIL_FROM_NAME', 'WeAnswerIt'),
                'from_email' => env('MAIL_FROM_ADDRESS', 'do-not-reply@weanswer.it'),
                'email' => env('APP_EMAIL', 'info@weanswer.it'),
                'support_email' => env('APP_SUPPORT_EMAIL', 'support@weanswer.it'),
            ],
        ];
    }

    public static function getMailConfiguration()
    {
        $themeSettings = static::find(static::EMAILS);

        $mailConfig = [];

        if ($themeSettings) {
            $values = (array)$themeSettings->value;

            foreach ($values as $key => $param) {
                if ($key == 'headingColor') {
                    $mailConfig['h1Color'] = $param;
                    $mailConfig['h2Color'] = $param;
                    $mailConfig['h3Color'] = $param;
                    $mailConfig['h4Color'] = $param;
                    $mailConfig['h5Color'] = $param;
                    $mailConfig['h6Color'] = $param;
                } elseif ($key == 'textColor') {
                    $mailConfig['pColor'] = $param;
                } else {
                    if (is_object($param)) {
                        $param = (array)$param;
                    }

                    $mailConfig[$key] = $param;
                }
            }
        }

        $logo = static::getLogo();

        if ($logo) {
            $fullWidth = false;

            if (isset($mailConfig['headerImgFullWidth'])) {
                $fullWidth = $mailConfig['headerImgFullWidth'];
            }

            $size = $fullWidth ? 680 : 200;

            $mailConfig['headerImgUrl'] = config('app.url') . $logo->getUrl(function (\ColorTools\Image $image) use ($size) {
                    $image->fillWidth($size);
                }, $logo->type == 'png' ? 'png' : 'jpeg');
        }

        return $mailConfig;
    }

    private static function getImages()
    {
        return static::find(static::MEDIA)->images;
    }

    public static function getMedia()
    {
        return ['images' => static::getImages()];
    }
}

<?php

namespace Wai\Adminify;

use Illuminate\Support\ServiceProvider;

class AdminifyServiceProvider extends ServiceProvider
{
    protected $commands = [
        'Wai\Adminify\Commands\SetupCommand',
        'Wai\Adminify\Commands\InstallCommand',
    ];

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__ . '/Routes/web.php');

        $this->loadViewsFrom(__DIR__ . '/Resources/views', 'adminify');
    }

    /**
     * Register the application services.
     *
     * @return void
     * @throws BindingResolutionException
     */
    public function register()
    {
        \Schema::defaultStringLength(191);

        $this->commands($this->commands);


        // Only reason this route is not in routes file. Is because it needs to be last for Vue to work.
        $this->registerCatchAllRoute();
    }

    /**
     * Registers the catch all Laravel route, allows Vue Router to handle routing.
     *
     * @return void
     */
    private function registerCatchAllRoute()
    {
        \App::booted(function () {
            app('router')
                ->middleware(['web', 'HTMLMin\HTMLMin\Http\Middleware\MinifyMiddleware'])
                ->get('{any}', '\App\Http\Controllers\AppController@any')
                ->where('any', '(.*)')
                ->name('catch-all');
        });
        $router = $this->app['router'];
        $router->pushMiddlewareToGroup('web', \Wai\Adminify\Middleware\Debugbar::class);
    }
}

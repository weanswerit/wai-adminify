<?php

namespace Wai\Adminify\Events;

use App\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class UpdateNotifications implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    private $user;

    /**
     * Create a new event instance.
     *
     * @param $user
     */
    public function __construct($user)
    {
        if (is_numeric($user)) {
            $this->user = User::find($user);
        } else {
            $this->user = $user;
        }
    }

    /**
     * The event's broadcast name.
     *
     * @return string
     */
    public function broadcastAs()
    {
        return 'update-notifications';
    }

    public function broadcastWith()
    {
        $notificationsCount = $this->user->notifications()->count();
        $unreadCount = $this->user->notifications()->whereNull('read_at')->count();
        return [
            'notifications' => $notificationsCount,
            'unread' => $unreadCount,
        ];
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {

        return new PrivateChannel('notifications.' . $this->user->id);
    }
}

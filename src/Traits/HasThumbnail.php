<?php

namespace Wai\Adminify\Traits;

use ColorTools\HasImages;
use Laravolt\Avatar\Avatar;
use Illuminate\Support\Str;

trait HasThumbnail
{
    public static $SLUG_SEPARATOR = ' 2@1 ';
    public static $SLUG_SEPARATOR_SLUGGED = '2At1';


    /**
     * Allowed extensions
     * @var array
     */
    protected $allowedImageTypes = ['jpeg', 'jpg', 'png'];

    /**
     * Retrieves a single image with the role thumbnail as the item's thumbnail.
     *
     * @return mixed
     * @throws \ReflectionException
     */
    public function getThumbnail()
    {
        $traits = (new \ReflectionClass(__CLASS__))->getTraits();

        if (in_array('ColorTools\HasImages', get_declared_traits())) {
            return $this->imageByRole('thumbnail');
        } else {
            return null;
        }
    }

    /**
     * Gets the thumbnail url
     * @param null $size
     * @return string
     * @throws \ReflectionException
     */
    public function getThumbnailUrl($size = null)
    {
        if (is_null($size)) {
            $size = config('avatar.width');
        }
        $model = self::classToSlug($this);

        if ($this->getThumbnail()) {
            return config('app.url') . $this->thumbnail->getUrl(function (\ColorTools\Image $image) use ($size) {
                    $image->fit($size, $size);
                }, 'jpeg');
        } else {
            return route('thumbnail', [$model, $this->{$this->primaryKey}]) . '-s=' . $size . 'x' . $size . '.jpeg';
        }
    }

    /**
     * Gets the thumbnail url attribute
     * @return string
     * @throws \ReflectionException
     */
    public function getThumbnailUrlAttribute()
    {
        return $this->getThumbnailUrl();
    }

    /**
     * Sets the thumbnail url
     * @return string
     */
    public function setThumbnailUrlAttribute()
    {
        return;
    }

    /**
     * Gets the thumbnail
     * @return array|string
     * @throws \ReflectionException
     */
    public function getThumbnailAttribute()
    {
        $thumbnail = $this->getThumbnail();

        if ($thumbnail) {
            $thumbnail->canDelete = true;
            return $thumbnail;
        } else {
            $model = self::classToSlug($this);
            return ['model' => $model, 'modelId' => $this->{$this->primaryKey}];
        }
    }

    /**
     * Sets the thumbnail
     * @return string
     */
    public function setThumbnailAttribute()
    {
        return;
    }

    /**
     * Gets the avatar string - can be overriden
     * @return string
     */
    public function getThumbnailString()
    {
        $string = '';

        if (isset($this->thumbnail_column)) {
            $string = (string) $this->{$this->thumbnail_column};
        } else {
            $string = (string) $this->name;
        }

        return $string;
    }

    /**
     * @param $string
     * @return \Laravolt\Avatar\Avatar
     */
    public function getThumbnailObject($string)
    {
        $thumbnail = new Avatar(config('avatar'));
        return $thumbnail->create($string);
    }

    /**
     * @param $modelInstance
     * @param $size
     * @param null $color
     * @return mixed
     */
    private function getModelThumbnailImage($modelInstance, $size, $color = null)
    {
        $thumbnail = $modelInstance->getThumbnailObject($modelInstance->getThumbnailString());
        $thumbnail->setDimension($size[0], $size[1]);
        $thumbnail->setFontSize(ceil($size[0] / 2));

        if ($color) {
            $color = '#' . substr(ltrim($color, '#'), 0, 6);
            $thumbnail->setBackground($color);
        }

        $image = $thumbnail->getImageObject();

        return $image->getCore();
    }

    /**
     * @param $thumbnailImage
     * @param $type
     */
    private function displayThumbnailImage($thumbnailImage, $type)
    {
        if ($type == 'png') {
            header('Content-Type: image/png');
            imagepng($thumbnailImage);
        } else {
            header('Content-Type: image/jpeg');
            imagejpeg($thumbnailImage, null, 90);
        }

        imagedestroy($thumbnailImage);
        exit();
    }

    /**
     * @param $modelInstance
     * @param $size
     * @param null $color
     * @param $type
     * @return
     */
    private function displayModelThumbnailImage($modelInstance, $size, $color = null, $type)
    {
        $thumbnailImage = $this->getModelThumbnailImage($modelInstance, $size, $color);

        return $this->displayThumbnailImage($thumbnailImage, $type);
    }

    /**
     * @param $modelParams
     * @return array
     */
    private function resolveParams($modelParams)
    {
        $queryString = explode('.', $modelParams);

        $type = end($queryString);

        $modifiers = explode('-', $queryString[0]);

        $modelId = $modifiers[0];

        $color = null;
        $size = [config('avatar.width'), config('avatar.width')];

        if (isset($modifiers[1]) and !empty($modifiers[1])) {
            $modifiers = explode('+', $modifiers[1]);

            foreach ($modifiers as $modifier) {
                if (empty($modifier)) {
                    continue;
                }

                $modifierParams = explode('=', $modifier);

                if (count($modifierParams) > 1) {
                    if ($modifierParams[0] == 's') {
                        $size = explode('x', $modifierParams[1]);
                    }

                    if ($modifierParams[0] == 'c') {
                        $color = $modifierParams[1];
                    }
                }
            }
        }

        return [
            'id' => $modelId,
            'color' => $color,
            'size' => $size,
            'type' => $type
        ];
    }

    /**
     * @param $model
     * @param $modelParams
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|void
     */
    public function resolveRequest($model, $modelParams)
    {
        if (!Str::endsWith($modelParams, $this->allowedImageTypes)) {
            $hasExtension = strpos($modelParams, '.');
            if ($hasExtension) {
                $modelParams = substr($modelParams, 0, $hasExtension);
            }

            $modelParams .= '.jpeg';

            return redirect(route('thumbnail', [$model, $modelParams]));
        }

        if ($model == 'random') {
            return $this->generateString($modelParams);
        }

        try {
            $class = self::resolveClassName($model);
        } catch (\Exception $e) {
            abort(404, $e->getMessage());
        }

        $params = $this->resolveParams($modelParams);

        $hasSoftDeletes = in_array(\Illuminate\Database\Eloquent\SoftDeletes::class, class_uses($class));

        if ($hasSoftDeletes) {
            $object = $class::withTrashed()->find($params['id']);
        } else {
            $object = $class::query()->find($params['id']);
        }

        if (!$object) {
            abort(404, 'Model not found');
        }

        return $this->displayModelThumbnailImage($object, $params['size'], $params['color'], $params['type']);
    }

    /**
     * @param $modelParams
     * @return void
     */
    private function generateString($modelParams)
    {
        $params = $this->resolveParams($modelParams);

        $thumbnail = new Avatar(config('avatar'));

        $name = strtoupper(substr($params['id'], 0, 2));

        $thumbnail = $thumbnail->create($name);
        $thumbnail->setDimension($params['size'][0], $params['size'][1]);
        $thumbnail->setFontSize(ceil($params['size'][0] / 2));

        if ($params['color']) {
            $color = '#' . substr(ltrim($params['color'], '#'), 0, 6);
            $thumbnail->setBackground($color);
        }

        $image = $thumbnail->getImageObject();
        $core = $image->getCore();

        return $this->displayThumbnailImage($core, $params['type']);
    }



    /**
     * @param $class
     * @return string
     */
    public static function classToSlug($class)
    {
        //$name = class_basename($class);
        $name = get_class($class);
        if(substr($name, '0', 4) == 'App\\') {
            $name = substr($name, 4);
        }
        $name = ltrim($name, '\\');
        $name = str_replace('\\', self::$SLUG_SEPARATOR, $name);

        $pieces = preg_split('/(?=[A-Z])/', $name);
        $humanReadableClassName = trim(implode(' ', $pieces));

        return Str::slug($humanReadableClassName, '_');
    }

    /**
     * @param $name
     * @return mixed
     * @throws \Exception
     */
    public static function resolveClassName($name)
    {
        $convertedName = explode('_', $name);
        $capitalizedName = array_map('ucfirst', $convertedName);
        $name = str_replace(self::$SLUG_SEPARATOR_SLUGGED, '\\', implode('', $capitalizedName));


        if(!class_exists($name)) {
            $name = 'App\\'.$name;
        }

        if(class_exists($name) and is_subclass_of($name, \Illuminate\Database\Eloquent\Model::class)) {
            return (new $name);
        }

        throw new \Exception('Cannot find class ' . $name);
    }
}

<?php

namespace Wai\Adminify\Traits;

use App\Log;

trait Loggable
{
    /**
     * @param $logType
     * @param array $details
     * @throws \ReflectionException
     * @throws \Exception
     */
    public function log($logType, $details = [])
    {
        if (!Log::checkIfValidLogType($logType)) {
            throw new \Exception('Cannot identify log type: "' . $logType . '"');
        }

        Log::create($logType, $this, null, $details);
    }

    /**
     * @param $logType
     * @param $user
     * @param array $details
     * @throws \ReflectionException
     * @throws \Exception
     */
    public function logAs($logType, $user, $details = [])
    {
        if (!Log::checkIfValidLogType($logType)) {
            throw new \Exception('Cannot identify log type: "' . $logType . '"');
        }

        Log::create($logType, $this, $user, $details);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function logs()
    {
        return $this->morphMany(Log::class, 'association')->orderBy('created_at', 'DESC');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Relations\MorphMany|object|null
     */
    public function getLastAction()
    {
        return $this->logs()->first();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Relations\MorphMany|object|null
     */
    public function getLastActionAttribute()
    {
        return $this->getLastAction();
    }

    /**
     * @return mixed
     */
    public function getLastActionTimeAttribute()
    {
        $lastAction = $this->getLastAction();
        if ($lastAction) {
            return $lastAction->created_at->toDateTimeString();
        }
    }

}

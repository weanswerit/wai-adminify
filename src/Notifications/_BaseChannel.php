<?php

namespace Wai\Adminify\Notifications;

use Illuminate\Notifications\Notification;

class _BaseChannel
{

    /**
     * @param $notifiable
     * @param Notification $notification
     * @return null
     */
    public function send($notifiable, Notification $notification)
    {
        $data = $notification->toDatabase($notifiable);
        $identifier = md5(json_encode($data));

        $notificationExists = \DB::table('notifications')
            ->where('notifiable_id', $notifiable->id)
            ->where('identifier', $identifier)
            ->where('read_at', null)
            ->exists();

        if ($notificationExists) {
            return null;
        }

        return $notifiable->routeNotificationFor('database')->create([
            'id' => $notification->id,
            'type' => get_class($notification),
            'data' => $data,
            'read_at' => null,
            'identifier' => $identifier
        ]);
    }

}

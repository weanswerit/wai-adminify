<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
$middleware = ['web', \HTMLMin\HTMLMin\Http\Middleware\MinifyMiddleware::class];

Route::group(['middleware' => $middleware], function () use ($middleware) {

    Route::group(['middleware' => array_merge($middleware, ['auth'])], function () {
        Route::post('/notifications/{any}', ['uses' => 'Wai\Adminify\Controllers\NotificationsController@vueHelper', 'as' => 'notifications']);
    });

    Route::get('/thumbnail/{model}/{modelId}/{size?}', ['uses' => 'Wai\Adminify\Controllers\ThumbnailController@generate', 'as' => 'thumbnail']);

    Route::get('/user-not-found/{email?}', ['uses' => 'Wai\Adminify\Controllers\Auth\LoginController@userMissing', 'as' => 'user.not-found']);

    Route::get('/favicon/manifest.json', ['uses' => 'Wai\Adminify\Controllers\AppController@favicon', 'as' => 'favicon_manifest']);
    Route::get('/favicon/browserconfig.xml', ['uses' => 'Wai\Adminify\Controllers\AppController@browserconfig', 'as' => 'favicon_browserconfig']);
});

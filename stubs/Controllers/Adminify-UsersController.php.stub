<?php

namespace App\Http\Controllers;

use App\Log;
use App\User;
use App\Role;
use App\Notification;
use Wai\Adminify\Events\UpdateDom;
use Wai\Adminify\Events\CacheClear;
use Wai\Adminify\Controllers\UsersController as Controller;

class UsersController extends Controller
{
    protected $class = User::class;
    protected $itemName = 'user';
    protected $orderAsc = false;

    protected $orderBy = 'created_at';
    protected $itemsPerPage = 15;

    protected $singleAppends = ['thumbnail', 'role_ids'];
    protected $multipleAppends = ['thumbnail'];

    protected $singleRelationships = ['images'];
    protected $multipleRelationships = ['roles'];

    protected $searchColumns = ['first_name', 'last_name', 'email'];

    /**
     * Custom search query for users.
     *
     * @param bool $trashedOnly
     * @return mixed
     */
    public function searchQuery($trashedOnly = false)
    {
        $filter = request('filter');
        $itemsPerPage = request('itemsPerPage');

        if ($itemsPerPage) {
            $this->itemsPerPage = $itemsPerPage;
        }

        $results = $this->class::query();

        if ($trashedOnly) {
            $results->onlyTrashed();
        }

        $results->with($this->multipleRelationships);

        if (isset($filter['searchQuery']) and !empty($filter['searchQuery'])) {
            $this->searchColumnsForText($results, $filter['searchQuery']);
        }

        if (isset($filter['filters']) and !empty($filter['filters'])) {
            $this->applyFilters($results, $filter['filters']);
        }

        $this->checkSortBy($results);

        return $results;
    }

    /**
     * Specify how to build search query with filter keys and return result query.
     *
     * @param $query
     * @param $filters
     * @return mixed
     */
    public function applyFilters($query, $filters)
    {
        foreach ($filters as $key => $ids) {
            if (empty($ids)) {
                continue;
            }
            switch ($key) {
                case 'roles':
                    $query->where(function ($subQuery) use ($ids) {
                        $subQuery->whereHas('roles', function ($q) use ($ids) {
                            $q->whereIn('id', $ids);
                        });
                    });
                    break;
                default:
                    break;
            }
        }

        return $query;
    }

    /**
     * Check if user with email already exists.
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function exists()
    {
        if (request('email')) {
            $email = $this->class::findByEmail(request('email'));

            if ($email && !request('id')) {
                return response('Email');
            } elseif ($email && request('id') && $email->id !== request('id')) {
                return response('Email');
            }
        }
    }

    /**
     * Logs when a item is fetched.
     *
     * @param $item
     */
    public function beforeGet($item)
    {
        $item->log(Log::LOG_USER_VIEWED);
    }

    /**
     * Logs when a item was created.
     *
     * @param $item
     */
    public function afterCreate($item)
    {
        $item->log(Log::LOG_USER_CREATED);

        Notification::notifyRoles('create' . Str::studly($this->itemName), $item, [Role::ADMIN, Role::SUPER_ADMIN])
            ->setVerb('created')
            ->dispatch();

        $this->dispatchEvents();
    }

    /**
     * Logs when a item was updated.
     *
     * @param $item
     */
    public function afterUpdate($item)
    {
        $item->log(Log::LOG_USER_UPDATED);

        Notification::notifyRoles('update' . Str::studly($this->itemName), $item, [Role::ADMIN, Role::SUPER_ADMIN])
            ->setVerb('updated')
            ->dispatch();

        $this->dispatchEvents();
    }

    /**
     * Logs when a item was deleted.
     *
     * @param $item
     */
    public function afterDelete($item)
    {
        $item->log(Log::LOG_USER_DELETED);

        Notification::notifyRoles('delete' . Str::studly($this->itemName), $item, [Role::ADMIN, Role::SUPER_ADMIN])
            ->setVerb('deleted')
            ->dispatch();

        $this->dispatchEvents();
    }

    /**
     * Logs when a item was restored.
     *
     * @param $item
     */
    public function afterRestore($item)
    {
        $item->log(Log::LOG_USER_RESTORED);

        Notification::notifyRoles('restore' . Str::studly($this->itemName), $item, [Role::ADMIN, Role::SUPER_ADMIN])
                    ->setVerb('restored')
                    ->dispatch();

        $this->dispatchEvents();
    }

    /**
     * Dispatches websocket events.
     *
     */
    private function dispatchEvents()
    {
        event(new CacheClear('users-store'));
        event(new CacheClear('users'));
        event(new UpdateDom('users'));
    }
}

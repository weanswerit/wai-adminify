const mix = require('laravel-mix');
const webpack = require('webpack');
const adminifyVariables = require('adminify/src/variables');
const VuetifyLoaderPlugin = require('vuetify-loader/lib/plugin');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
    .js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'css/app.css')
    .sass('resources/sass/pre-loader.scss', 'css/pre-loader.css')
    .webpackConfig({
        plugins: [
            new VuetifyLoaderPlugin(),
            new webpack.DefinePlugin(adminifyVariables),
        ]
    })
    .version();

if (!mix.config.production) {
    mix.sourceMaps();
}

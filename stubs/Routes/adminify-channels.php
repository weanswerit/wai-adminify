<?php

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

Broadcast::channel('update-dom', function () {
    return true;
});

Broadcast::channel('cache-clear', function () {
    return true;
});

Broadcast::channel('notifications.{userId}', function ($user, $userId) {
    return $user->id == $userId;
});

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

$middleware = [\HTMLMin\HTMLMin\Http\Middleware\MinifyMiddleware::class];

Route::group(['middleware' => $middleware], function () {

    // Login Routes...
    Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
    Route::post('/login', 'Auth\LoginController@login');

    // Logout Route...
    Route::post('/logout', 'Auth\LoginController@logout')->name('logout');
    Route::get('/logout', 'Auth\LoginController@logout')->name('logout');

    // Google auth routes
    if (config('auth.social_login')) {
        Route::get('/login/google', ['uses' => 'Auth\SocialAuthController@loginGoogle']);
        Route::get('/callback/google', ['uses' => 'Auth\SocialAuthController@callbackGoogle']);
    }

    // Register Routes...
    if (config('auth.register')) {
        Route::get('/register', 'Auth\RegisterController@showRegistrationForm')->name('register');
        Route::post('/register', 'Auth\RegisterController@register');
    }

    // Password Reset Routes...
    Route::get('/password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
    Route::post('/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::get('/password/reset/{email}/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
    Route::post('/password/reset', 'Auth\ResetPasswordController@reset');

    // Email Validation Routes...
    if (config('auth.email_verification')) {
        Route::get('/email/verify', 'Auth\VerificationController@show')->name('verification.notice');
        Route::get('/email/verify/{id}/{token}', 'Auth\VerificationController@verify')->name('verification.verify');
        Route::post('/email/resend', 'Auth\VerificationController@resend')->name('verification.resend');
    }

});

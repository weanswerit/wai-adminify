<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
$middleware = [\HTMLMin\HTMLMin\Http\Middleware\MinifyMiddleware::class];

Route::group(['middleware' => $middleware], function () {

    Route::get('/', ['uses' => 'AppController@index', 'as' => 'home']);

});


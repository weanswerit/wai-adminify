<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
$middleware = [\HTMLMin\HTMLMin\Http\Middleware\MinifyMiddleware::class];

Route::group(['middleware' => $middleware], function () {

    Route::get('/', ['uses' => 'AppController@index', 'as' => 'home']);
    Route::post('/token', ['uses' => 'AppController@token', 'as' => 'token']);

    Route::group(['middleware' => 'auth'], function () {
        Route::post('/users/{any}', ['uses' => 'UsersController@vueHelper', 'as' => 'users']);

        Route::post('/resources/{any}', ['uses' => 'ResourcesController@vueHelper', 'as' => 'resources']);

        Route::post('/images/{any}', ['uses' => 'ImagesController@vueHelper', 'as' => 'images']);

        Route::post('/files/{any}', ['uses' => 'FilesController@vueHelper', 'as' => 'files']);

        Route::post('/logs/{any}', ['uses' => 'LogsController@vueHelper', 'as' => 'logs']);
        Route::post('/settings/{any}', ['uses' => 'SettingsController@vueHelper', 'as' => 'settings']);

        Route::get('/impersonate/{id}', ['uses' => 'UsersController@impersonateLogin']);
        Route::get('/impersonate-return', ['uses' => 'UsersController@impersonateReturn']);
    });
});

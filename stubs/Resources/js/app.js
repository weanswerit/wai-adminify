require('./bootstrap');
require('adminify/src/helpers');
require('./components');

import Vue from 'vue';
import App from 'adminify/src/App';
import router from './router';
import store from './store';

if (process.env.NODE_ENV === 'production') {
    Vue.config.devtools = false;
    Vue.config.debug = false;
    Vue.config.silent = true;
}

router.beforeEach((to, from, next) => {
    store.dispatch('application/clearFilters');
    store.dispatch('application/resetTopBar');
    next();
});

import Vuetify from "vuetify";

Vue.use(Vuetify);

import * as mdi from '@mdi/js';

new Vue({
    data() {
        return {
            rules: {
                email: value => {
                    if (!value.length) {
                        return true;
                    }
                    const pattern = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
                    return pattern.test(value) || 'Invalid e-mail.'
                }
            }
        }
    },
    el: '#app',
    router,
    vuetify: new Vuetify({
        icons: {
            iconfont: 'mdiSvg',
            values: mdi
        },
    }),
    store,
    template: '<App/>',
    components: {App},
    mounted() {
    },
    created() {
    },
});

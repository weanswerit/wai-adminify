import Vue from 'vue';
import Vuex from 'vuex';
import {store} from 'adminify';

Vue.use(Vuex);

export default new Vuex.Store(store());

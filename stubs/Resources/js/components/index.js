import Vue from 'vue';

Vue.component('v-node-table', require('adminify/src/components/Tables/Index').default);
Vue.component('v-sub-table', require('adminify/src/components/Tables/Sub-Index').default);

Vue.component('v-confirm', require('adminify/src/components/Dialogs/ConfirmDialog').default);
Vue.component('v-create', require('adminify/src/components/Dialogs/CreateDialog').default);

Vue.component('v-img-editor', require('adminify/src/components/Media/ImageEditor').default);

Vue.component('v-thumbnail', require('adminify/src/components/Media/Thumbnail').default);
Vue.component('v-thumbnail-editor', require('adminify/src/components/Media/ThumbnailEditor').default);
Vue.component('v-single-image-by-role', require('adminify/src/components/Media/SingleImageByRole').default);

Vue.component('v-upload', require('adminify/src/components/Media/Upload').default);

Vue.component('draggable', require('vuedraggable'));

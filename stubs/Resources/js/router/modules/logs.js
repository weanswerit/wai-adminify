export default [
    {
        path: '/logs',
        component: require('adminify/src/functional/Index').default,
        meta: {
            breadcrumb: 'Logs'
        },
        children: [
            {
                path: '',
                name: 'Logs',
                component: require('../../views/logs/Index').default
            }
        ]
    }
]

export default [
    {
        path: '/settings',
        component: require('adminify/src/functional/Index').default,
        meta: {
            breadcrumb: 'Settings'
        },
        children: [
            {
                path: '',
                name: 'Settings',
                component: require('../../views/settings/List').default
            },
        ]
    }
]

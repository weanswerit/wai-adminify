export default [
    {
        path: '/users',
        component: require('adminify/src/functional/Index').default,
        meta: {
            breadcrumb: 'Users'
        },
        children: [
            {
                path: '',
                name: 'Users',
                component: require('../../views/users/Index').default
            },
            {
                path: 'edit/:userId',
                meta: {
                    breadcrumb: 'Edit User'
                },
                component: require('adminify/src/functional/Index').default,
                children: [
                    {
                        path: '',
                        name: 'Edit User',
                        component: require('../../views/users/Edit').default
                    }
                ]
            },
            {
                path: 'deleted',
                meta: {
                    breadcrumb: 'Deleted users'
                },
                component: require('adminify/src/functional/Index').default,
                children: [
                    {
                        path: '',
                        name: 'Deleted users',
                        component: require('../../views/users/Deleted').default
                    }
                ]
            }
        ]
    },
    {
        path: '/profile',
        component: require('adminify/src/functional/Index').default,
        meta: {
            breadcrumb: 'Profile'
        },
        children: [
            {
                path: '',
                name: 'Profile',
                component: require('../../views/users/Edit').default
            },
        ]
    }
]

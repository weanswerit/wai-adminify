export default [
    {
        path: '/image-store',
        component: require('adminify/src/functional/Index').default,
        meta: {
            breadcrumb: 'Images'
        },
        children: [
            {
                path: '',
                name: 'Images',
                component: require('../../views/media/Images/Index').default
            },
            {
                path: 'edit/:imageId',
                meta: {
                    breadcrumb: 'Edit Image'
                },
                component: require('adminify/src/functional/Index').default,
                children: [
                    {
                        path: '',
                        name: 'Edit Image',
                        component: require('../../views/media/Images/Edit').default
                    }
                ]
            },
        ]
    },
    {
        path: '/file-store',
        component: require('adminify/src/functional/Index').default,
        meta: {
            breadcrumb: 'Files'
        },
        children: [
            {
                path: '',
                name: 'Files',
                component: require('../../views/media/Files/Index').default
            },
            {
                path: 'edit/:fileId',
                meta: {
                    breadcrumb: 'Edit File'
                },
                component: require('adminify/src/functional/Index').default,
                children: [
                    {
                        path: '',
                        name: 'Edit File',
                        component: require('../../views/media/Files/Edit').default
                    }
                ]
            },
        ]
    }
]

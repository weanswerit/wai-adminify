import userRoutes from './modules/users';
import mediaRoutes from './modules/media';
import logRoutes from './modules/logs';
import settingRoutes from './modules/settings';

let routes = [
    ...userRoutes,
    ...mediaRoutes,
    ...logRoutes,
    ...settingRoutes,
    {
        path: '/dashboard',
        name: 'Dashboard',
        component: require('../views/Dashboard.vue').default,
        meta: {
            breadcrumb: 'Dashboard'
        }
    }
];

import Vue from 'vue';
import Router from 'vue-router';
import {router} from 'adminify';

let routerObject = router(routes);

Vue.use(Router);

export default new Router(routerObject);

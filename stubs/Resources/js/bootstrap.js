window._ = require('lodash');

window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}

window.moment = require('moment');

window.domurl = require('domurl');

import Highcharts from 'highcharts';
import HighChartsOptions from './highchartOptions.json';

Highcharts.setOptions(HighChartsOptions);

require('highcharts/highcharts-3d')(Highcharts);
require('highcharts/highcharts-more')(Highcharts);

import HighchartsVue from 'highcharts-vue';
Vue.use(HighchartsVue);

import Vue from 'vue';

import Toasted from 'vue-toasted';
Vue.use(Toasted);

import VideoBg from 'vue-videobg';
Vue.component('video-bg', VideoBg);

import InfiniteLoading from 'vue-infinite-loading';
Vue.use(InfiniteLoading);

window.fileDownload = require('js-file-download');

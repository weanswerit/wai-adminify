let menu = [
    {
        'heading': 'Dashboard',
        'icon': '$mdiViewDashboard',
        'children': [
            {
                'icon': '$mdiViewDashboard',
                'name': 'Dashboard',
                'path': '/dashboard',
                'restriction': 'none'
            }
        ],
        'restriction': 'none'
    },
    {
        'heading': 'Users',
        'icon': '$mdiAccountGroup',
        'children': [
            {
                'icon': '$mdiShieldAccount',
                'name': 'Users',
                'path': '/users?page=1',
            }
        ],
        'restriction': 'admin|super-admin'
    },
    {
        'heading': 'Media',
        'icon': '$mdiEyeSettings',
        'children': [
            {
                'icon': '$mdiImageOutline',
                'name': 'Images',
                'path': '/image-store?page=1',
            },
            {
                'icon': '$mdiFilePdf',
                'name': 'Files',
                'path': '/file-store?page=1',
            }
        ],
        'restriction': 'admin|super-admin'
    },
    {
        'heading': 'Logs and settings',
        'icon': '$mdiKeyVariant',
        'children': [
            {
                'icon': '$mdiClipboardText',
                'name': 'Logs',
                'path': '/logs?page=1',
            },
            {
                'icon': '$mdiSettings',
                'name': 'Settings',
                'path': '/settings',
            }
        ],
        'restriction': 'admin|super-admin'
    },
];

module.exports = menu;

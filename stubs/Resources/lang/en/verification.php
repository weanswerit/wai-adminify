<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Verification Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during verification for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'verified' => 'Already verified. Please refresh the page.',
    'sent' => 'Email verification sent.',

];

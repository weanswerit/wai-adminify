<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'Your password has been reset!',
    'sent' => 'If the information entered is associated with an account, then we have sent you an email with password reset instructions.',
    'token' => 'This password reset token is invalid.',
    'user' => 'If the information entered is associated with an account, then we have sent you an email with password reset instructions.',
    'throttled' => 'Please wait before retrying.',

];
